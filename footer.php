<footer>
    <section class="footers wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="top-footers">
            <div class="container">
                <div class="row gutter-156">
                    <div class="col-lg-6">
                        <div class="box-footers">
                            <div class="logo-footers mb-25s">
                                <a href="#" title="">
                                    <img src="theme/assets/images/logo-footers.svg" alt="">
                                </a>
                            </div>
                            <h3 class="titles-bold__alls titles-footers__alls fs-20s mb-25s">Công ty TNHH ĐT và PT nha khoa Việt Nam</h3>
                            <ul class="list-intros__footers mb-25s">
                                <li>
                                    <p><span class="titles-bold__alls">Địa chỉ: </span> 50 ngõ 14 Vũ Hữu, Thanh Xuân, Hà Nội</p>
                                </li>
                                <li>
                                    <p><span class="titles-bold__alls">Email:</span> <a href="" title="">vanhien.nhakhoa@gmail.com</a></p>
                                </li>
                            </ul>
                            <a href="#" title="" class="phones-footers titles-bold__alls color-blues"><img src="theme/assets/images/phone-headers.svg"> 1-800 123-456</a>
                            <div class="app-footers">
                                <h3 class="titles-bold__alls titles-footers__alls fs-16s mb-20s">Theo dõi chúng tôi:</h3>
                                <ul class="list-apps__footers">
                                    <li>
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-app-footer-1.svg" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-app-footer-2.svg" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-app-footer-3.svg" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-app-footer-4.svg" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div class="box-footers">
                            <h3 class="titles-bold__alls titles-footers__alls fs-16s mb-25s">Tìm hiểu thêm</h3>
                            <ul class="list-nav__footers">
                                <li>
                                    <a href="abouts.php" title="">Giới thiệu</a>
                                </li>
                                <li>
                                    <a href="banggia.php" title="">Bảng giá</a>
                                </li>
                                <li>
                                    <a href="tintuc-kienthucnhakhoa.php" title="">Tin tức nha khoa</a>
                                </li>
                                <li>
                                    <a href="contacts.php" title="">Liên hệ</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div class="box-footers">
                            <h3 class="titles-bold__alls titles-footers__alls fs-16s mb-25s">Dịch vụ</h3>
                            <ul class="list-nav__footers">
                                <li>
                                    <a href="dichvuniengrang.php" title="">Niềng răng</a>
                                </li>
                                <li>
                                    <a href="dichvunhorangkhon.php" title="">Nhổ răng khôn</a>
                                </li>
                                <li>
                                    <a href="dichvuthammirangsu.php" title="">Thẩm mỹ răng sứ</a>
                                </li>
                                <li>
                                    <a href="dichvudieutrirangdau.php" title="">Điều trị răng đau</a>
                                </li>
                                <li>
                                    <a href="dichvutrongranggia.php" title="">Trồng răng giả</a>
                                </li>
                                <li>
                                    <a href="dichvunhakhoakhac.php" title="">Dịch vụ nha khoa khác</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footers">
            <div class="container">
                <p class="copy-rights__footers">Design by @GCO 2022</p>
            </div>
        </div>
    </section>
    <div class="modal fade modals-accounts__alls" id="modal-guides__accounts" tabindex="-1" role="dialog" aria-labelledby="modal-guides-modals" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="tops-modals__guides mb-30s">
                    <h3 class="fs-32s color-blues-seconds">Các ca nổi bật</h3>
                    <ul class="zoom-btn__modals">
                        <li>
                            <p><img src="theme/assets/images/btn-zoom-in-modals.svg"></p>
                        </li>
                        <li>
                            <p><img src="theme/assets/images/btn-zoom-out-modals.svg"></p>
                        </li>
                    </ul>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="theme/assets/images/clear-modals-alls.png" alt="">
                    </button>
                </div>
                <div class="intros-guides__accounts">
                    <div class="slide-guides__accounts">
                        <div class="swiper sl-guides__accounts mb-30s">
                            <div class=" swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="items-guides__accounts">
                                        <div class="img-guides__accounts">
                                            <img alt="" class="cloudzoom" src="theme/assets/images/img-big-modals.png">
                                        </div>
                                        <p class="title-images__shows fs-15s titles-center__alls">Chữa răng sâu</p>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-guides__accounts">
                                        <div class="img-guides__accounts">
                                            <img alt="" class="cloudzoom" src="theme/assets/images/img-big-modals.png">
                                        </div>
                                        <p class="title-images__shows fs-15s titles-center__alls">Chữa răng sâu</p>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-guides__accounts">
                                        <div class="img-guides__accounts">
                                            <img alt="" class="cloudzoom" src="theme/assets/images/img-big-modals.png">
                                        </div>
                                        <p class="title-images__shows fs-15s titles-center__alls">Chữa răng sâu</p>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-guides__accounts">
                                        <div class="img-guides__accounts">
                                            <img alt="" class="cloudzoom" src="theme/assets/images/img-big-modals.png">
                                        </div>
                                        <p class="title-images__shows fs-15s titles-center__alls">Chữa răng sâu</p>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-guides__accounts">
                                        <div class="img-guides__accounts">
                                            <img alt="" class="cloudzoom" src="theme/assets/images/img-big-modals.png">
                                        </div>
                                        <p class="title-images__shows fs-15s titles-center__alls">Chữa răng sâu</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-pagination fraction"></div>
                        </div>
                        <div class="group-btns__showss">
                            <div class="showss-button-prev"><img src="theme/assets/images/arowns-btn-modals.svg" alt=""></div>
                            <div class="showss-button-next"><img src="theme/assets/images/arowns-btn-modals.svg" alt=""></div>
                        </div>
                        <div class="sl-thums__guides swiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="shows-thumb__smalls">
                                        <img alt="" src="theme/assets/images/img-thumb-modals-bottoms-3.png" alt="">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="shows-thumb__smalls">
                                        <img alt="" src="theme/assets/images/img-thumb-modals-bottoms-2.png" alt="">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="shows-thumb__smalls">
                                        <img alt="" src="theme/assets/images/img-thumb-modals-bottoms-3.png" alt="">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="shows-thumb__smalls">
                                        <img alt="" src="theme/assets/images/img-thumb-modals-bottoms-4.png" alt="">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="shows-thumb__smalls">
                                        <img alt="" src="theme/assets/images/img-thumb-modals-bottoms-5.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade modals-book__alls" id="modal-book__mains" tabindex="-1" role="dialog" aria-labelledby="modal-guides-modals" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="form-book__mains">
                    <h3 class="titles-bold__alls titles-transform__alls titles-book__forms fs-24s">Đặt lịch hẹn</h3>
                    <form>
                        <div class="row gutter-16">
                            <div class="col-lg-12">
                                <div class="form-groups__book">
                                    <input type="text" name="" placeholder="Tên của bạn*" class="control-alls input-alls">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-groups__book">
                                    <input type="text" name="" placeholder="Số điện thoại*" class="control-alls input-alls">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="control-select__alls">
                                    <select class="select-alls">
                                        <option value="">Dịch vụ*</option>
                                        <option value="1">Niềng răng</option>
                                        <option value="2">Nhổ răng khôn</option>
                                        <option value="3">Thẩm mỹ răng sứ</option>
                                        <option value="3">Điều trị răng đau</option>
                                        <option value="3">Trồng răng giả</option>
                                        <option value="3">Dịch vụ nha khoa khác</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-groups__book">
                                    <input type="text" name="" placeholder="Độ tuổi" class="control-alls input-alls">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="control-select__alls">
                                    <select class="select-alls">
                                        <option value="">Giới tính</option>
                                        <option value="1">Nam</option>
                                        <option value="2">Nữ</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="control-groups__accounts control-date">
                                    <input placeholder="Ngày" class="textbox-n" type="date" />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="control-select__alls">
                                    <select class="select-alls">
                                        <option value="">Giờ</option>
                                        <option value="1">19 giờ</option>
                                        <option value="2">21 giờ</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <textarea rows="3" class="control-alls control-texts" placeholder="Tình trạng"></textarea>
                            </div>
                        </div>
                        <a href="index.php" title="" href="" class="btn-oranges__alls"> <i class="fa fa-calendar-o" aria-hidden="true"></i> Đặt lịch</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <ul class="groups-fixed__alls">
        <li><a href=""><img src="theme/assets/images/icons-app-fix-1.png" alt=""></a></li>
        <li><a href=""><img src="theme/assets/images/icons-app-fix-2.png" alt=""></a></li>
        <li><a href=""><img src="theme/assets/images/icons-app-fix-3.png" alt=""></a></li>
        <li><button class="up-to__tops"><i class="fa fa-angle-double-up" aria-hidden="true"></i></button></li>
        <li>
            <a href="#" title="" class="phones-rings__fixed">
                <div class="icons-phone__fixeds">
                    <span>
                        <i class="fa fa-phone" aria-hidden="true"></i>
                    </span>
                </div>
                <span class="number-phone__rings">
                    0906 250 068
                </span>
            </a>
        </li>
    </ul>
    <script src="theme/assets/js/jquery-3.4.1.min.js" defer></script>
    <script src="theme/assets/js/moment.min.js" defer></script>
    <script src="theme/assets/js/wow.min.js" defer></script>
    <script src="theme/assets/js/select-2.js" defer></script>
    <script src="theme/assets/js/bootstrap.min.js" defer></script>
    <script src="theme/assets/js/swiper.min.js" defer></script>
    <script src="theme/assets/js/jquery.fancybox.min.js" defer></script>
    <script src="theme/assets/js/script.js" defer></script>
    </body>

    </html>