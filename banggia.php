<?php @include('header.php'); ?>
<main>
    <section class="contents-price__lists">
        <div class="container mb-25s">
            <ul class="breadcrumb">
                <li><a href="index.php" title="">Trang chủ</a></li>
                <li><a title="" class="active">Trang bảng giá</a></li>
            </ul>
        </div>
        <div class="container mb-100s wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
            <div class="top-price__list">
                <div class="titles-before__mains mb-50s">
                    <h2 class="titles-transform__alls titles-bold__alls color-blues-seconds fs-36s "> Bảng giá </h2>
                </div>
                <div class="img-price__lists mb-25s">
                    <img src="theme/assets/images/img-tops-price-list.png" alt="">
                </div>
                <div class="intros-prices__lists">
                    <div class="row">
                        <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                            <div class="btn-classify__prices">
                                <img src="theme/assets/images/img-classify-btn-price.png" alt="">
                                <span>Nhổ răng khôn</span>
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                <ul class="list-classify__pricess">
                                    <li>
                                        <a href="#" class="active">Niềng răng</a>
                                    </li>
                                    <li>
                                        <a href="#">Nhổ răng khôn</a>
                                    </li>
                                    <li>
                                        <a href="#">Thẩm mỹ răng sứ</a>
                                    </li>
                                    <li>
                                        <a href="#">Điều trị răng đau</a>
                                    </li>
                                    <li>
                                        <a href="#">Trồng răng giả</a>
                                    </li>
                                    <li>
                                        <a href="#">Dịch vụ nha khoa khác</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                            <h3 class="titles-bold__alls titles-transform__alls color-blues-seconds fs-18s">Giá</h3>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                            Răng khôn hàm trên mọc thẳng
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                            <p>3.500.000-4.500.000đ</p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                            <button data-toggle="modal" data-target="#modal-book__mains" title="" class="btn-greys__alls"> Đặt lịch </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                            Răng khôn hàm dưới mọc thẳng
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                            <p>3.500.000-4.500.000đ</p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                            <button data-toggle="modal" data-target="#modal-book__mains" title="" class="btn-greys__alls"> Đặt lịch </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                            Răng khôn hàm trên mọc lệch
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                            <p>3.500.000-4.500.000đ</p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                            <button data-toggle="modal" data-target="#modal-book__mains" title="" class="btn-greys__alls"> Đặt lịch </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                            Răng khôn hàm dưới mọc lệch
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                            <p>3.500.000-4.500.000đ</p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                            <button data-toggle="modal" data-target="#modal-book__mains" title="" class="btn-greys__alls"> Đặt lịch </button>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section class="price-your__care wow fadeInUp">
        <div class="container">
            <div class="infos-price__care">
                <h2 class="titles-bold__alls fs-20s titles-transform__alls color-blues-seconds">Bảng giá dịch vụ khác</h2>
                <ul class="list-price__cares">
                    <li>
                        <a href="#" title="">Bảng giá niềng răng</a>
                    </li>
                    <li>
                        <a href="#" title="">Bảng giá thẩm mỹ răng sứ</a>
                    </li>
                    <li>
                        <a href="#" title="">Bảng giá điều trị răng đau</a>
                    </li>
                    <li>
                        <a href="#" title="">Bảng giá dịch vụ nha khoa khác</a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>