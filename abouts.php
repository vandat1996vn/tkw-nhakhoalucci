<?php @include('header.php'); ?>
<main>
    <section class="container mb-25s">
        <ul class="breadcrumb">
            <li><a href="index.php" title="">Trang chủ</a></li>
            <li><a title="" class="active">Giới thiệu</a></li>
        </ul>
    </section>
    <section class=" container mb-100s">
        <div class="banner-alls__pages">
            <img src="theme/assets/images/banner-page-1.png" alt="">
        </div>
    </section>
    <section class="videos-about__pages">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="text-about__mains">
                        <h2 class="titles-transform__alls fs-20s color-blues-seconds"> Về chúng tôi </h2>
                        <h3 class="titles-transform__alls titles-bold__alls color-blues-seconds fs-40s mb-30s"> nha khoa Lucci</h3>
                        <div class="text-padding__abouts">
                            <p class="titles-bold__alls color-blues-seconds text-before__mains mb-20s">Nisl vitae quisque sapien id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper arcu quis dui nunc at blandit tincidunt in viverra.</p>
                            <p class="mb-40s">Nisl vitae quisque sapien id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper arcu quis dui nunc at blandit tincidunt in viverra. Convallis ac mauris ut egestas est mauris. Tellus volutpat vitae suspendisse sit semper. Nisi duis consequat pellentesque nulla urna integer turpis. Vestibulum morbi a in et. Diam urna nam a vitae. Turpis eu nec morbi dapibus. Nisl vitae quisque sapien id. Hendrerit eget morbi nisl tellus tortor odio amet fermentum turpis.</p>
                            <div class="groups-btn__afters">
                                <button data-toggle="modal" data-target="#modal-book__mains" class="btn-blues__second"><i class="fa fa-calendar-o" aria-hidden="true"></i> đặt lịch khám</button>
                                <img src="theme/assets/images/after-btn-alls.png" title="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="videos-about__mains">
                        <img src="theme/assets/images/img-videos-abouts.png" alt="">
                        <a href="https://youtu.be/g20t_K9dlhU" data-fancybox="video" class="btn-videos__abouts">
                            <img src="theme/assets/images/btn-videos-blues.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="abouts-sevice__pages wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="container">
            <div class="top-abouts__sevice mb-60s">
                <div>
                    <h2 class="titles-transform__alls fs-20s color-blues-seconds">Nha khoa lucci </h2>
                    <h3 class="titles-transform__alls titles-bold__alls color-blues-seconds fs-40s "> Dịch vụ cung cấp</h3>
                </div>
                <div class="text-top__abouts">
                    <p class="titles-bold__alls color-blues-seconds">Nisl vitae quisque sapien id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper arcu quis dui nunc at blandit tincidunt in viverra.</p>
                </div>
            </div>
            <div class="list-abouts__sevices">
                <div class="row gutter-40">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="items-abouts__sevices">
                            <div class="intros-abouts__sevices">
                                <img src="theme/assets/images/img-sevice-abouts-1.png" alt="">
                                <h3><a href="dichvuniengrang.php" class="names-abouts__sevices titles-bold__alls fs-22s">Niềng răng - Chỉnh nha</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="items-abouts__sevices">
                            <div class="intros-abouts__sevices">
                                <img src="theme/assets/images/img-sevice-abouts-4.png" alt="">
                                <h3><a href="dichvunhorangkhon.php" class="names-abouts__sevices titles-bold__alls fs-22s">Nhổ răng khôn</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="items-abouts__sevices">
                            <div class="intros-abouts__sevices">
                                <img src="theme/assets/images/img-sevice-abouts-2.png" alt="">
                                <h3><a href="dichvuthammirangsu.php" class="names-abouts__sevices titles-bold__alls fs-22s">Thẩm mỹ răng sứ</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="items-abouts__sevices">
                            <div class="intros-abouts__sevices">
                                <img src="theme/assets/images/img-sevice-abouts-6.png" alt="">
                                <h3><a href="dichvudieutrirangdau.php" class="names-abouts__sevices titles-bold__alls fs-22s">Điều trị răng đau</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="items-abouts__sevices">
                            <div class="intros-abouts__sevices">
                                <img src="theme/assets/images/img-sevice-abouts-5.png" alt="">
                                <h3><a href="dichvutrongranggia.php" class="names-abouts__sevices titles-bold__alls fs-22s">Trồng răng giả</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="items-abouts__sevices">
                            <div class="intros-abouts__sevices">
                                <img src="theme/assets/images/img-sevice-abouts-3.png" alt="">
                                <h3><a href="dichvunhakhoakhac.php" class="names-abouts__sevices titles-bold__alls fs-22s">Dịch vụ nha khoa khác</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about-vision mb-120s wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="container">
            <div class="row gutter-60">
                <div class="col-lg-6">
                    <div class="slide-abouts__visions ">
                        <div class="sl-abouts__visions swiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="items-abouts__visions">
                                        <a data-toggle="modal" data-target="#modal-guides__accounts" title="">
                                            <img src="theme/assets/images/img-abouts-visionss.png">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-abouts__visions">
                                        <a data-toggle="modal" data-target="#modal-guides__accounts" title="">
                                            <img src="theme/assets/images/img-abouts-visionss.png">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-abouts__visions">
                                        <a data-toggle="modal" data-target="#modal-guides__accounts" title="">
                                            <img src="theme/assets/images/img-abouts-visionss.png">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                        <div class="group-btns__showss">
                            <div class="showss-button-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                            <div class="showss-button-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="text-about__mains">
                        <h2 class="titles-transform__alls color-blues-seconds fs-20s"> Giá trị cốt lõi </h2>
                        <h3 class="titles-transform__alls titles-bold__alls color-blues-seconds fs-40s mb-30s"> Tầm nhìn - sứ mệnh </h3>
                        <div class="text-padding__abouts">
                            <p class="titles-bold__alls color-blues-seconds text-before__mains mb-20s">Nisl vitae quisque sapien id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper arcu quis dui nunc at blandit tincidunt in viverra.</p>
                            <p class="mb-20s"><span class="titles-bold__alls color-blues-seconds mb-20s">Tầm nhìn chiến lược :</span> Consectetur adipiscing elit. Ullamcorper arcu quis dui nunc at blandit tincidunt in viverra. Convallis ac mauris ut egestas est mauris. Tellus volutpat vitae suspendisse sit semper. Nisi duis consequat pellentesque nulla urna integer turpis. Vestibulum morbi a in et. Diam urna nam a vitae. Turpis eu nec morbi dapibus. Nisl vitae quisque sapien id. Hendrerit eget morbi nisl tellus tortor odio amet fermentum turpis.</p>
                            <p class="mb-20s"><span class="titles-bold__alls color-blues-seconds mb-20s">Sứ mệnh :</span> Nisi duis consequat pellentesque nulla urna integer turpis. Vestibulum morbi a in et. Diam urna nam a vitae. Turpis eu nec morbi dapibus. Nisl vitae quisque sapien id. Hendrerit eget morbi nisl tellus tortor odio amet fermentum turpis. Diam urna nam a vitae</p>
                            <div class="data-abouts__visions">
                                <div class="items-data__visions">
                                    <img src="theme/assets/images/data-vision-1.png">
                                    <p class="number-abouts__visions titles-bold__alls fs-40s color-blues-seconds">2560</p>
                                    <p class="fs-13s titles-bold__alls titles-transform__alls">Khách hàng</p>
                                </div>
                                <div class="items-data__visions">
                                    <img src="theme/assets/images/data-vision-2.png">
                                    <p class="number-abouts__visions titles-bold__alls fs-40s color-blues-seconds">6500+</p>
                                    <p class="fs-13s titles-bold__alls titles-transform__alls">Nhãn hàng</p>
                                </div>
                                <div class="items-data__visions">
                                    <img src="theme/assets/images/data-vision-3.png">
                                    <p class="number-abouts__visions titles-bold__alls fs-40s color-blues-seconds">10</p>
                                    <p class="fs-13s titles-bold__alls titles-transform__alls">Cơ sở</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sl-images__visions swiper mb-100s wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="items-images__visions">
                    <img src="theme/assets/images/images-vision-about-1.png" alt="">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="items-images__visions">
                    <img src="theme/assets/images/images-vision-about-2.png" alt="">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="items-images__visions">
                    <img src="theme/assets/images/images-vision-about-3.png" alt="">
                </div>
            </div>
            <div class="swiper-slide">
                <div class="items-images__visions">
                    <img src="theme/assets/images/images-vision-about-4.png" alt="">
                </div>
            </div>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>