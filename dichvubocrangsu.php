<?php @include('header.php'); ?>
<main>
    <section class="container mb-25s">
        <ul class="breadcrumb">
            <li><a href="index.php" title="">Trang chủ</a></li>
            <li><a href="index.php" title="">Dịch vụ</a></li>
            <li><a title="" class="active">Dịch vụ nha khoa khác</a></li>
        </ul>
    </section>
    <section class="container mb-70s wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="banner-sevice__details mb-20s">
            <img src="theme/assets/images/img-banner-sevide-1.png">
        </div>
        <ul class="list-sevice__navs">
            <li>
                <a href="dichvuniengrang.php" title="">Niềng răng</a>
            </li>
            <li>
                <a href="dichvunhorangkhon.php" title="">Nhổ răng khôn</a>
            </li>
            <li>
                <a href="dichvuthammirangsu.php" title="">Thẩm mỹ răng sứ</a>
            </li>
            <li>
                <a href="dichvudieutrirangdau.php" title="">Điều trị răng đau</a>
            </li>
            <li>
                <a href="dichvutrongranggia.php" title="">Trồng răng giả</a>
            </li>
            <li>
                <a href="dichvunhakhoakhac.php" title="" class="active">Dịch vụ nha khoa khác</a>
            </li>
        </ul>
    </section>
    <section class="container mb-120s wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="text-introduce__sevices">
            <div class="titles-before__mains mb-50s">
                <h2 class="titles-transform__alls  color-blues-seconds fs-36s"><span class="titles-bold__alls">Bọc răng sứ</span></h2>
            </div>
            <div class="intros-text__sevices">
                <p>Bệnh nhân cần được chụp hình để biết được răng khôn mọc đúng hay sai, bác sĩ có thể kê toa thuốc giảm đau và thuốc kháng sinh giảm sưng viêm mô mềm cho bệnh nhân. Trường hợp răng mọc sai cần được nhổ bỏ để tránh ảnh hưởng nghiêm trọng đến các răng khác.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi purus, pretium tristique elit vestibulum. Nunc dictum molestie nibh amet mauris morbi facilisis. Viverra risus eu suscipit pharetra elementum, massa magna nibh lacus. Metus erat quis quisque consectetur eget. Erat fringilla pharetra tristique ut non. Imperdiet velit, vestibulum scelerisque eget posuere magna lectus. Nulla dictum facilisi velit dolor sed purus eget ut neque. Arcu sed sit porttitor faucibus tellus amet amet, cras. Eget orci ut morbi id pellentesque diam arcu eget. A lacus, habitasse tempus justo, consectetur maecenas sit. Vel vitae turpis iaculis ante placerat sit feugiat nisl, facilisi. Metus pretium posuere maecenas facilisis eu ultricies. Eget leo, pharetra est sapien feugiat mus. Urna urna, dui at ridiculus eget tristique cras. A lorem nulla est, ac. Felis, interdum sit accumsan in at sagittis varius. Scelerisque eu convallis eget at rutrum at dui. Sed habitasse tellus pellentesque risus volutpat auctor tincidunt. Elit nam duis sit faucibus egestas posuere erat lacus.</p>
                <br>
                <img src="theme/assets/images/img-bottoms-sevice-text-2.png" alt="">
                <br>
                <br>
                <p>Amet, consectetur adipiscing elit. Morbi purus, pretium tristique elit vestibulum. Nunc dictum molestie nibh amet mauris morbi facilisis. Viverra risus eu suscipit pharetra elementum, massa magna nibh lacus. Metus erat quis quisque consectetur eget. Erat fringilla pharetra tristique ut non. Imperdiet velit, vestibulum scelerisque eget posuere magna lectus. Nulla dictum facilisi velit dolor sed purus eget ut neque. Arcu sed sit porttitor faucibus tellus amet amet, cras. Eget orci ut morbi id pellentesque diam arcu eget. A lacus, habitasse tempus justo, consectetur maecenas sit. Vel vitae turpis iaculis ante placerat sit feugiat nisl, facilisi. Metus pretium posuere maecenas facilisis eu ultricies. Eget leo, pharetra est sapien feugiat mus. Urna urna, dui at ridiculus eget tristique cras. A lorem nulla est, ac. Felis, interdum sit accumsan in at sagittis varius. Scelerisque eu convallis eget at rutrum at dui. Sed habitasse tellus pellentesque risus volutpat auctor tincidunt. Elit nam duis sit faucibus egestas posuere erat lacus.</p>
                <br>
                <img src="theme/assets/images/img-bottoms-sevice-text-3.png" alt="">
                <br>
                <br>
                <p>Amet, consectetur adipiscing elit. Morbi purus, pretium tristique elit vestibulum. Nunc dictum molestie nibh amet mauris morbi facilisis. Viverra risus eu suscipit pharetra elementum, massa magna nibh lacus. Metus erat quis quisque consectetur eget. Erat fringilla pharetra tristique ut non. Imperdiet velit, vestibulum scelerisque eget posuere magna lectus. Nulla dictum facilisi velit dolor sed purus eget ut neque. Arcu sed sit porttitor faucibus tellus amet amet, cras. Eget orci ut morbi id pellentesque diam arcu eget. A lacus, habitasse tempus justo, consectetur maecenas sit. Vel vitae turpis iaculis ante placerat sit feugiat nisl, facilisi. Metus pretium posuere maecenas facilisis eu ultricies. Eget leo, pharetra est sapien feugiat mus. Urna urna, dui at ridiculus eget tristique cras. A lorem nulla est, ac. Felis, interdum sit accumsan in at sagittis varius. Scelerisque eu convallis eget at rutrum at dui. Sed habitasse tellus pellentesque risus volutpat auctor tincidunt. Elit nam duis sit faucibus egestas posuere erat lacus.</p>
            </div>
            <div class="bottom-news__details">
                <div class="apps-news__details">
                    <h3 class="titles-bold__alls color-blues-seconds fs-16s mb-20s">Chia sẻ</h3>
                    <ul class="app-footers__details">
                        <li>
                            <a href="#" title="">
                                <img src="theme/assets/images/img-app-footer-1.svg" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="#" title="">
                                <img src="theme/assets/images/img-app-footer-2.svg" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="#" title="">
                                <img src="theme/assets/images/img-app-footer-3.svg" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
                <button title="" class="btn-blues__second mb-40s" data-toggle="modal" data-target="#modal-book__mains"><i class="fa fa-calendar-o" aria-hidden="true"></i> Đặt lịch khám</button>
            </div>
        </div>
    </section>
    <section class="price-your__care wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="container">
            <div class="infos-price__care">
                <h2 class="titles-bold__alls fs-20s titles-transform__alls color-blues-seconds">Có thể bạn quan tâm</h2>
                <ul class="list-price__cares">
                    <li>
                        <a href="dichvunhakhoakhac.php" title="">Bảng giá niềng răng</a>
                    </li>
                    <li>
                        <a href="dichvunhakhoakhac.php" title="">Bảng giá thẩm mỹ răng sứ</a>
                    </li>
                    <li>
                        <a href="dichvunhakhoakhac.php" title="">Bảng giá điều trị răng đau</a>
                    </li>
                    <li>
                        <a href="dichvunhakhoakhac.php" title="">Bảng giá dịch vụ nha khoa khác</a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="book-calendar__contacts wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="container">
            <form>
                <h2 class="titles-bold__alls titles-transform__alls titles-center__alls color-blues-seconds fs-48s mb-60s">Đặt lịch khám</h2>
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <div class="form-groups__book">
                            <input type="text" name="" placeholder="Tên của bạn*" class="control-alls input-alls">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-groups__book">
                            <input type="text" name="" placeholder="Số điện thoại*" class="control-alls input-alls">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-groups__book">
                            <input type="text" name="" placeholder="Độ tuổi*" class="control-alls input-alls">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="control-select__alls">
                            <select class="select-alls">
                                <option value="">Dịch vụ *</option>
                                <option value="1">Niềng răng</option>
                                <option value="2">Nhổ răng khôn</option>
                                <option value="3">Thẩm mỹ răng sứ</option>
                                <option value="3">Điều trị răng đau</option>
                                <option value="3">Trồng răng giả</option>
                                <option value="3">Dịch vụ nha khoa khác</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="control-groups__accounts mb-10s control-date">
                            <input placeholder="Ngày" class="textbox-n" type="date" />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="control-select__alls">
                            <select class="select-alls">
                                <option value="">Giờ</option>
                                <option value="1">Niềng răng</option>
                                <option value="2">Nhổ răng khôn</option>
                                <option value="3">Thẩm mỹ răng sứ</option>
                                <option value="3">Điều trị răng đau</option>
                                <option value="3">Trồng răng giả</option>
                                <option value="3">Dịch vụ nha khoa khác</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="5" class="control-alls control-texts" placeholder="Tình trạng"></textarea>
                    </div>
                </div>
                <button class="btn-oranges__alls"><i class="fa fa-calendar-o" aria-hidden="true"></i> Đặt lịch</button>
                <div class="bg-contacts__pages">
                    <img src="theme/assets/images/bg-contacts-pages-form.png" alt="">
                </div>
            </form>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>