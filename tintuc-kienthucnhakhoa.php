<?php @include('header.php'); ?>
<main>
    <section class="container mb-25">
        <ul class="breadcrumb">
            <li><a href="index.php" title="">Trang chủ</a></li>
            <li><a title="" class="active">Kiến thức nha khoa</a></li>
        </ul>
    </section>
    <section class="content-news__pages">
        <div class="container">
            <h2 class="titles-bold__alls titles-transform__alls color-blues-seconds fs-40s mb-40s">Kiến thức nha khoa</h2>
            <div class="row gutter-70">
                <div class="col-lg-9 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="items-news__hots">
                        <div class="img-news__hots mb-25s">
                            <a href="tintuc-chitiet.php" title="">
                                <img src="theme/assets/images/img-news-hots-1.png" alt="">
                            </a>
                        </div>
                        <div class="intros-news__hots">
                            <h3><a href="tintuc-chitiet.php" title="" class="names-news__hots titles-bold__alls titles-transform__alls fs-24s mb-10s">Thông tin chi tiết về kỹ thuật niềng răng bằng máng nhựa!</a></h3>
                            <ul class="views-times__alls mb-20s">
                                <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                <li><img src="theme/assets/images/img-views-calendar-2.png" alt=""> 455 lượt xem </li>
                            </ul>
                            <p class="mb-20s">Niềng răng bằng máng nhựa đang là phương pháp được nhiều khách hàng quan tâm do tính thẩm mỹ cao và hiệu quả chỉnh nha tốt. Thế nhưng, không phải ai cũng hiểu rõ về kỹ thuật niềng răng này. Bài viết dưới đây sẽ cung cấp cho bạn những thông tin chi tiết về niềng răng bằng máng nhựa. Cùng tìm hiểu nhé!</p>
                            <a href="tintuc-chitiet.php" title="" class="btn-blues__trans fs-14s">Xem chi tiết</a>
                        </div>
                    </div>
                    <div class="list-news__pages mb-50s">
                        <div class="items-news__pages">
                            <div class="img-news__pages">
                                <a href="tintuc-chitiet.php" title="">
                                    <img src="theme/assets/images/img-news-pages-1.png" alt="">
                                </a>
                            </div>
                            <div class="intros-news__pages">
                                <h3><a href="tintuc-chitiet.php" title="" class="titles-bold__alls names-news__pages fs-20s mb-10s">Thông tin chi tiết về kỹ thuật niềng răng bằng máng nhựa!</a></h3>
                                <ul class="views-times__alls mb-20s">
                                    <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    <li><img src="theme/assets/images/img-views-calendar-2.png" alt=""> 455 lượt xem </li>
                                </ul>
                                <p class="mb-20s">Niềng răng bằng máng nhựa đang là phương pháp được nhiều khách hàng quan tâm do tính thẩm mỹ cao và hiệu quả chỉnh nha tốt. Thế nhưng, không phải ai cũng hiểu rõ về kỹ thuật niềng răng này...</p>
                                <a href="tintuc-chitiet.php" title="" class="btn-blues__trans fs-14s">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="items-news__pages">
                            <div class="img-news__pages">
                                <a href="tintuc-chitiet.php" title="">
                                    <img src="theme/assets/images/img-news-pages-2.png" alt="">
                                </a>
                            </div>
                            <div class="intros-news__pages">
                                <h3><a href="tintuc-chitiet.php" title="" class="titles-bold__alls names-news__pages fs-20s mb-10s">Thông tin chi tiết về kỹ thuật niềng răng bằng máng nhựa!</a></h3>
                                <ul class="views-times__alls mb-20s">
                                    <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    <li><img src="theme/assets/images/img-views-calendar-2.png" alt=""> 455 lượt xem </li>
                                </ul>
                                <p class="mb-20s">Niềng răng bằng máng nhựa đang là phương pháp được nhiều khách hàng quan tâm do tính thẩm mỹ cao và hiệu quả chỉnh nha tốt. Thế nhưng, không phải ai cũng hiểu rõ về kỹ thuật niềng răng này...</p>
                                <a href="tintuc-chitiet.php" title="" class="btn-blues__trans fs-14s">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="items-news__pages">
                            <div class="img-news__pages">
                                <a href="tintuc-chitiet.php" title="">
                                    <img src="theme/assets/images/img-news-pages-3.png" alt="">
                                </a>
                            </div>
                            <div class="intros-news__pages">
                                <h3><a href="tintuc-chitiet.php" title="" class="titles-bold__alls names-news__pages fs-20s mb-10s">Thông tin chi tiết về kỹ thuật niềng răng bằng máng nhựa!</a></h3>
                                <ul class="views-times__alls mb-20s">
                                    <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    <li><img src="theme/assets/images/img-views-calendar-2.png" alt=""> 455 lượt xem </li>
                                </ul>
                                <p class="mb-20s">Niềng răng bằng máng nhựa đang là phương pháp được nhiều khách hàng quan tâm do tính thẩm mỹ cao và hiệu quả chỉnh nha tốt. Thế nhưng, không phải ai cũng hiểu rõ về kỹ thuật niềng răng này...</p>
                                <a href="tintuc-chitiet.php" title="" class="btn-blues__trans fs-14s">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="items-news__pages">
                            <div class="img-news__pages">
                                <a href="tintuc-chitiet.php" title="">
                                    <img src="theme/assets/images/img-news-pages-4.png" alt="">
                                </a>
                            </div>
                            <div class="intros-news__pages">
                                <h3><a href="tintuc-chitiet.php" title="" class="titles-bold__alls names-news__pages fs-20s mb-10s">Thông tin chi tiết về kỹ thuật niềng răng bằng máng nhựa!</a></h3>
                                <ul class="views-times__alls mb-20s">
                                    <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    <li><img src="theme/assets/images/img-views-calendar-2.png" alt=""> 455 lượt xem </li>
                                </ul>
                                <p class="mb-20s">Niềng răng bằng máng nhựa đang là phương pháp được nhiều khách hàng quan tâm do tính thẩm mỹ cao và hiệu quả chỉnh nha tốt. Thế nhưng, không phải ai cũng hiểu rõ về kỹ thuật niềng răng này...</p>
                                <a href="tintuc-chitiet.php" title="" class="btn-blues__trans fs-14s">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="items-news__pages">
                            <div class="img-news__pages">
                                <a href="tintuc-chitiet.php" title="">
                                    <img src="theme/assets/images/img-news-pages-5.png" alt="">
                                </a>
                            </div>
                            <div class="intros-news__pages">
                                <h3><a href="tintuc-chitiet.php" title="" class="titles-bold__alls names-news__pages fs-20s mb-10s">Thông tin chi tiết về kỹ thuật niềng răng bằng máng nhựa!</a></h3>
                                <ul class="views-times__alls mb-20s">
                                    <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    <li><img src="theme/assets/images/img-views-calendar-2.png" alt=""> 455 lượt xem </li>
                                </ul>
                                <p class="mb-20s">Niềng răng bằng máng nhựa đang là phương pháp được nhiều khách hàng quan tâm do tính thẩm mỹ cao và hiệu quả chỉnh nha tốt. Thế nhưng, không phải ai cũng hiểu rõ về kỹ thuật niềng răng này...</p>
                                <a href="tintuc-chitiet.php" title="" class="btn-blues__trans fs-14s">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="items-news__pages">
                            <div class="img-news__pages">
                                <a href="tintuc-chitiet.php" title="">
                                    <img src="theme/assets/images/img-news-pages-6.png" alt="">
                                </a>
                            </div>
                            <div class="intros-news__pages">
                                <h3><a href="tintuc-chitiet.php" title="" class="titles-bold__alls names-news__pages fs-20s mb-10s">Thông tin chi tiết về kỹ thuật niềng răng bằng máng nhựa!</a></h3>
                                <ul class="views-times__alls mb-20s">
                                    <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    <li><img src="theme/assets/images/img-views-calendar-2.png" alt=""> 455 lượt xem </li>
                                </ul>
                                <p class="mb-20s">Niềng răng bằng máng nhựa đang là phương pháp được nhiều khách hàng quan tâm do tính thẩm mỹ cao và hiệu quả chỉnh nha tốt. Thế nhưng, không phải ai cũng hiểu rõ về kỹ thuật niềng răng này...</p>
                                <a href="tintuc-chitiet.php" title="" class="btn-blues__trans fs-14s">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="items-news__pages">
                            <div class="img-news__pages">
                                <a href="tintuc-chitiet.php" title="">
                                    <img src="theme/assets/images/img-news-pages-7.png" alt="">
                                </a>
                            </div>
                            <div class="intros-news__pages">
                                <h3><a href="tintuc-chitiet.php" title="" class="titles-bold__alls names-news__pages fs-20s mb-10s">Thông tin chi tiết về kỹ thuật niềng răng bằng máng nhựa!</a></h3>
                                <ul class="views-times__alls mb-20s">
                                    <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    <li><img src="theme/assets/images/img-views-calendar-2.png" alt=""> 455 lượt xem </li>
                                </ul>
                                <p class="mb-20s">Niềng răng bằng máng nhựa đang là phương pháp được nhiều khách hàng quan tâm do tính thẩm mỹ cao và hiệu quả chỉnh nha tốt. Thế nhưng, không phải ai cũng hiểu rõ về kỹ thuật niềng răng này...</p>
                                <a href="tintuc-chitiet.php" title="" class="btn-blues__trans fs-14s">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="items-news__pages">
                            <div class="img-news__pages">
                                <a href="tintuc-chitiet.php" title="">
                                    <img src="theme/assets/images/img-news-pages-8.png" alt="">
                                </a>
                            </div>
                            <div class="intros-news__pages">
                                <h3><a href="tintuc-chitiet.php" title="" class="titles-bold__alls names-news__pages fs-20s mb-10s">Thông tin chi tiết về kỹ thuật niềng răng bằng máng nhựa!</a></h3>
                                <ul class="views-times__alls mb-20s">
                                    <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    <li><img src="theme/assets/images/img-views-calendar-2.png" alt=""> 455 lượt xem </li>
                                </ul>
                                <p class="mb-20s">Niềng răng bằng máng nhựa đang là phương pháp được nhiều khách hàng quan tâm do tính thẩm mỹ cao và hiệu quả chỉnh nha tốt. Thế nhưng, không phải ai cũng hiểu rõ về kỹ thuật niềng răng này...</p>
                                <a href="tintuc-chitiet.php" title="" class="btn-blues__trans fs-14s">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="items-news__pages">
                            <div class="img-news__pages">
                                <a href="tintuc-chitiet.php" title="">
                                    <img src="theme/assets/images/img-news-pages-9.png" alt="">
                                </a>
                            </div>
                            <div class="intros-news__pages">
                                <h3><a href="tintuc-chitiet.php" title="" class="titles-bold__alls names-news__pages fs-20s mb-10s">Thông tin chi tiết về kỹ thuật niềng răng bằng máng nhựa!</a></h3>
                                <ul class="views-times__alls mb-20s">
                                    <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    <li><img src="theme/assets/images/img-views-calendar-2.png" alt=""> 455 lượt xem </li>
                                </ul>
                                <p class="mb-20s">Niềng răng bằng máng nhựa đang là phương pháp được nhiều khách hàng quan tâm do tính thẩm mỹ cao và hiệu quả chỉnh nha tốt. Thế nhưng, không phải ai cũng hiểu rõ về kỹ thuật niềng răng này...</p>
                                <a href="tintuc-chitiet.php" title="" class="btn-blues__trans fs-14s">Xem chi tiết</a>
                            </div>
                        </div>
                    </div>
                    <div class="pagenigation mb-100s">
                        <a href="#" title="" class="page-items active">1</a>
                        <a href="#" title="" class="page-items"> 2 </a>
                        <a href="#" title="" class="page-items"> 3 </a>
                        <a href="#" title="" class="page-items"> 4 </a>
                        <a href="#" title="" class="page-items next-end"> <i class="fa fa-angle-right" aria-hidden="true"></i> </a>
                    </div>
                </div>
                <div class="col-lg-3 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="news-topic__sidebars mb-60s">
                        <h2 class="titles-bold__alls titles-transform__alls color-blues-seconds fs-20s mb-25s">Chủ đề</h2>
                        <div class="list-news__topic">
                            <div class="items-news__topics">
                                <div class="img-news__topics">
                                    <a href="tintuc-chitiet.php" title="">
                                        <img src="theme/assets/images/img-news-topics-1.png" alt="">
                                    </a>
                                </div>
                                <div class="intros-news__topics">
                                    <h3><a href="tintuc-chitiet.php" title="" class="names-news__topics titles-md__alls fs-16s mb-5s">Kiến thức nha khoa</a></h3>
                                    <p>1236 bài viết</p>
                                </div>
                            </div>
                            <div class="items-news__topics">
                                <div class="img-news__topics">
                                    <a href="tintuc-chitiet.php" title="">
                                        <img src="theme/assets/images/img-news-topics-2.png" alt="">
                                    </a>
                                </div>
                                <div class="intros-news__topics">
                                    <h3><a href="tintuc-chitiet.php" title="" class="names-news__topics titles-md__alls fs-16s mb-5s">Ca bệnh nổi bật</a></h3>
                                    <p>526 bài viết</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="news-hots__sidebars">
                        <h2 class="titles-bold__alls titles-transform__alls color-blues-seconds fs-20s mb-25s">bài viết nổi bật</h2>
                        <div class="list-hot__sidebars">
                            <div class="items-hots__sidebars">
                                <div class="img-news__topics">
                                    <a href="tintuc-chitiet.php" title="">
                                        <img src="theme/assets/images/img-news-hot-sidebars-1.png" alt="">
                                    </a>
                                </div>
                                <div class="intros-news__topics">
                                    <h3><a href="tintuc-chitiet.php" title="" class="names-news__topics titles-md__alls fs-16s mb-5s">Thông tin chi tiết về kỹ thuật niềng răng bằng máng nhựa!</a></h3>
                                    <ul class="views-times__alls mb-20s">
                                        <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="items-hots__sidebars">
                                <div class="img-news__topics">
                                    <a href="tintuc-chitiet.php" title="">
                                        <img src="theme/assets/images/img-news-hot-sidebars-2.png" alt="">
                                    </a>
                                </div>
                                <div class="intros-news__topics">
                                    <h3><a href="tintuc-chitiet.php" title="" class="names-news__topics titles-md__alls fs-16s mb-5s">Trồng răng implant có bị hôi miệng không?</a></h3>
                                    <ul class="views-times__alls mb-20s">
                                        <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="items-hots__sidebars">
                                <div class="img-news__topics">
                                    <a href="tintuc-chitiet.php" title="">
                                        <img src="theme/assets/images/img-news-hot-sidebars-3.png" alt="">
                                    </a>
                                </div>
                                <div class="intros-news__topics">
                                    <h3><a href="tintuc-chitiet.php" title="" class="names-news__topics titles-md__alls fs-16s mb-5s">Tìm hiểu về 2 phương pháp trồng răng hàm bị sâu!</a></h3>
                                    <ul class="views-times__alls mb-20s">
                                        <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="items-hots__sidebars">
                                <div class="img-news__topics">
                                    <a href="tintuc-chitiet.php" title="">
                                        <img src="theme/assets/images/img-news-hot-sidebars-4.png" alt="">
                                    </a>
                                </div>
                                <div class="intros-news__topics">
                                    <h3><a href="tintuc-chitiet.php" title="" class="names-news__topics titles-md__alls fs-16s mb-5s">25 tuổi chưa mọc răng khôn có sao không?</a></h3>
                                    <ul class="views-times__alls mb-20s">
                                        <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="items-hots__sidebars">
                                <div class="img-news__topics">
                                    <a href="tintuc-chitiet.php" title="">
                                        <img src="theme/assets/images/img-news-hot-sidebars-5.png" alt="">
                                    </a>
                                </div>
                                <div class="intros-news__topics">
                                    <h3><a href="tintuc-chitiet.php" title="" class="names-news__topics titles-md__alls fs-16s mb-5s">Khớp cắn đối đầu và những phương pháp điều trị hiệu quả</a></h3>
                                    <ul class="views-times__alls mb-20s">
                                        <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>