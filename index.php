<?php @include('header.php'); ?>
<main>
    <section class="banner-mains wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="bg-banner__mains">
            <img src="theme/assets/images/bg-banner-mains.png" alt="">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="intros-banner__mains">
                        <h2 class="titles-transform__alls fs-56s mb-25s">Nha khoa <span class="titles-bold__alls"> lucci
                                Mang đến nụ cười </span></h2>
                        <button href="#" data-toggle="modal" data-target="#modal-book__mains" class="btn-blues__second"><i class="fa fa-calendar-o" aria-hidden="true"></i> đặt lịch khám</button>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="img-banner__mains">
                        <img src="theme/assets/images/animation-1.png" alt="">
                        <div class="animations-banners">
                            <img src="theme/assets/images/animation-2.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="book-mains" id="book-mains-alls">
        <div class="container">
            <div class="row gutter-300">
                <div class="col-lg-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="book-videos__mains">
                        <div class="img-book-videos">
                            <img src="theme/assets/images/bg-animation-3.png" alt="">
                            <div class="img-book__animations">
                                <img src="theme/assets/images/bg-animation-4.png" alt="">
                            </div>
                        </div>
                        <a href="https://youtu.be/g20t_K9dlhU" data-fancybox="video" title="" class="btn-book__videos">
                            <img src="theme/assets/images/book-videos-mains-btn.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="form-book__mains">
                        <h3 class="titles-bold__alls titles-transform__alls titles-book__forms fs-24s">Đặt lịch hẹn</h3>
                        <form>
                            <div class="row gutter-16">
                                <div class="col-lg-12">
                                    <div class="form-groups__book">
                                        <input type="text" name="" placeholder="Tên của bạn*" class="control-alls input-alls">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-groups__book">
                                        <input type="text" name="" placeholder="Số điện thoại*" class="control-alls input-alls">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="control-select__alls">
                                        <select class="select-alls">
                                            <option value="">Dịch vụ*</option>
                                            <option value="1">Niềng răng</option>
                                            <option value="2">Nhổ răng khôn</option>
                                            <option value="3">Thẩm mỹ răng sứ</option>
                                            <option value="3">Điều trị răng đau</option>
                                            <option value="3">Trồng răng giả</option>
                                            <option value="3">Dịch vụ nha khoa khác</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-groups__book">
                                        <input type="text" name="" placeholder="Độ tuổi" class="control-alls input-alls">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="control-select__alls">
                                        <select class="select-alls">
                                            <option value="">Giới tính</option>
                                            <option value="1">Nam</option>
                                            <option value="2">Nữ</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="control-groups__accounts control-date">
                                        <input placeholder="Ngày" class="textbox-n" type="date" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="control-select__alls">
                                        <select class="select-alls">
                                            <option value="">Giờ</option>
                                            <option value="1">19 giờ</option>
                                            <option value="2">21 giờ</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <textarea rows="3" class="control-alls control-texts" placeholder="Tình trạng"></textarea>
                                </div>
                            </div>
                            <button class="btn-oranges__alls">Đặt lịch</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="why-me__mains wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="container">
            <div class="titles-before__mains mb-50s">
                <h2 class="titles-transform__alls  color-blues-seconds fs-36s"><span class="titles-bold__alls">Vì sao</span> chọn chúng tôi</h2>
            </div>
            <div class="row gutter-50">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-why-mains-1.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Niềng răng - Chỉnh nha</h3>
                        <div class="text-why__mains">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sagittis et commodo enim sed tellus erat facilisi rhoncus. Sapien sed consequat
                                euismod nunc. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-why-mains-2.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Thẩm mỹ răng sứ</h3>
                        <div class="text-why__mains">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sagittis et commodo enim sed tellus erat facilisi rhoncus. Sapien sed consequat
                                euismod nunc. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-why-mains-3.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Nhổ răng</h3>
                        <div class="text-why__mains">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sagittis et commodo enim sed tellus erat facilisi rhoncus. Sapien sed consequat
                                euismod nunc. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-why-mains-4.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Điều trị răng đau</h3>
                        <div class="text-why__mains">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sagittis et commodo enim sed tellus erat facilisi rhoncus. Sapien sed consequat
                                euismod nunc. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about-mains">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="text-about__mains">
                        <h2 class="titles-transform__alls color-blues-seconds fs-36s mb-30s">Về nha khoa <span class="titles-bold__alls"> Lucci </span></h2>
                        <div class="text-padding__abouts">
                            <p class="titles-bold__alls color-blues-seconds text-before__mains mb-20s">Nisl vitae quisque sapien id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper arcu quis dui nunc at blandit tincidunt in viverra.</p>
                            <p class="mb-40s">Nisl vitae quisque sapien id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper arcu quis dui nunc at blandit tincidunt in viverra. Convallis ac mauris ut egestas est mauris. Tellus volutpat vitae suspendisse sit semper. Nisi duis consequat pellentesque nulla urna integer turpis. Vestibulum morbi a in et. Diam urna nam a vitae. Turpis eu nec morbi dapibus. Nisl vitae quisque sapien id. Hendrerit eget morbi nisl tellus tortor odio amet fermentum turpis.</p>
                            <div class="groups-btn__afters">
                                <a href="abouts.php" title="" class="btn-blues__second mb-40s">Tìm hiểu thêm</a>
                                <img src="theme/assets/images/after-btn-alls.png" title="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="videos-about__mains">
                        <img src="theme/assets/images/img-abouts-mains-1.png" alt="">
                        <a href="https://youtu.be/g20t_K9dlhU" data-fancybox="video" class="btn-videos__abouts">
                            <img src="theme/assets/images/btn-videos-blues.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sevice-hot__mains mb-100s wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="list-sevice__hots">
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-1.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-2.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-3.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-4.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-5.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-6.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-7.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-8.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-9.png">
            </a>
        </div>
    </section>
    <section class="custoner-experience wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="container">
            <div class="titles-before__mains mb-50s">
                <h2 class="titles-transform__alls  color-blues-seconds fs-36s"><span class="titles-bold__alls">Khách hàng</span> Trải nghiệm</h2>
            </div>
            <div class="slide-customer__mains ">
                <div class="sl-customer__mains swiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="items-customers__mains">
                                <div class="img-customer__experiences mb-25s">
                                    <img src="theme/assets/images/img-customer-reviews.png" alt="">
                                </div>
                                <p class="mb-25s">Tôi rất hài lòng với nha khoa Lucci. Thật ra, tôi đã đến rất nhiều nha khoa, nhưng đây là địa chỉ tôi lựa chọn là bến đỗ lâu dài, tôi gắn bó với nha khoa Lucci 7-8 năm nay rồi. Tôi cũng đưa mọi người trong gia đình đến đây khám chữa răng. Tôi rất hài lòng với chuyên môn và chất lượng dịch vụ tại nha khoa. Tôi mong rằng đây sẽ không chỉ là địa chỉ tin cậy</p>
                                <p class="titles-bold__alls fs-20s color-blues-seconds">Anh Nguyễn Tuấn Khải</p>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="items-customers__mains">
                                <div class="img-customer__experiences mb-25s">
                                    <img src="theme/assets/images/img-customer-reviews.png" alt="">
                                </div>
                                <p class="mb-25s">Tôi rất hài lòng với nha khoa Lucci. Thật ra, tôi đã đến rất nhiều nha khoa, nhưng đây là địa chỉ tôi lựa chọn là bến đỗ lâu dài, tôi gắn bó với nha khoa Lucci 7-8 năm nay rồi. Tôi cũng đưa mọi người trong gia đình đến đây khám chữa răng. Tôi rất hài lòng với chuyên môn và chất lượng dịch vụ tại nha khoa. Tôi mong rằng đây sẽ không chỉ là địa chỉ tin cậy</p>
                                <p class="titles-bold__alls fs-20s color-blues-seconds">Anh Nguyễn Tuấn Khải</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
                <div class="group-btns__showss">
                    <div class="showss-button-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                    <div class="showss-button-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                </div>
            </div>
        </div>
    </section>
    <section class="news-mains wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="container">
            <div class="titles-before__mains mb-50s">
                <h2 class="titles-transform__alls  color-blues-seconds fs-36s"><span class="titles-bold__alls">Tin tức </span> nha khoa</h2>
            </div>
            <div class="slide-news__mains ">
                <div class="sl-news__mains swiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="items-news__mains">
                                <div class="img-news__mains">
                                    <a href="tintuc-chitiet.php" title="">
                                        <img src="theme/assets/images/img-news-main-1.png" alt="">
                                    </a>
                                </div>
                                <div class="intros-news__mains">
                                    <h3><a href="tintuc-chitiet.php" title="" class="names-news__mains titles-bold__alls fs-20s mb-10s">Tìm hiểu về 2 phương pháp trồng răng hàm bị sâu!</a></h3>
                                    <ul class="views-times__alls">
                                        <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                        <li><img src="theme/assets/images/img-views-calendar-2.png" alt=""> 455 lượt xem </li>
                                    </ul>
                                    <div class="text-news__mains">
                                        <p>Răng hàm bị sâu nếu không thể trám lại có thể phục hình bằng các phương pháp trồng răng để đảm bảo thẩm mỹ và chức năng ăn nhai.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="items-news__mains">
                                <div class="img-news__mains">
                                    <a href="tintuc-chitiet.php" title="">
                                        <img src="theme/assets/images/img-news-main-2.png" alt="">
                                    </a>
                                </div>
                                <div class="intros-news__mains">
                                    <h3><a href="tintuc-chitiet.php" title="" class="names-news__mains titles-bold__alls fs-20s mb-10s">Thông tin chi tiết về kỹ thuật niềng răng bằng máng nhựa!</a></h3>
                                    <ul class="views-times__alls">
                                        <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                        <li><img src="theme/assets/images/img-views-calendar-2.png" alt=""> 455 lượt xem </li>
                                    </ul>
                                    <div class="text-news__mains">
                                        <p>Niềng răng bằng máng nhựa đang là phương pháp được nhiều khách hàng quan tâm do tính thẩm mỹ cao và hiệu quả chỉnh nha</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="items-news__mains">
                                <div class="img-news__mains">
                                    <a href="tintuc-chitiet.php" title="">
                                        <img src="theme/assets/images/img-news-main-3.png" alt="">
                                    </a>
                                </div>
                                <div class="intros-news__mains">
                                    <h3><a href="tintuc-chitiet.php" title="" class="names-news__mains titles-bold__alls fs-20s mb-10s">25 tuổi chưa mọc răng khôn có sao không?</a></h3>
                                    <ul class="views-times__alls">
                                        <li><img src="theme/assets/images/img-views-calendar-1.png" alt=""> 18/06/2022 </li>
                                        <li><img src="theme/assets/images/img-views-calendar-2.png" alt=""> 455 lượt xem </li>
                                    </ul>
                                    <div class="text-news__mains">
                                        <p>Răng khôn (răng số 8) là răng mọc chậm nhất trên cung hàm. Lớp men trên mầm răng khôn hình thành từ lúc 8 - 10 tuổi,</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
                <div class="group-btns__showss">
                    <div class="showss-button-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                    <div class="showss-button-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>