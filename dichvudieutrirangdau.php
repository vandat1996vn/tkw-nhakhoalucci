<?php @include('header.php'); ?>
<main>
    <section class="container mb-25s">
        <ul class="breadcrumb">
            <li><a href="index.php" title="">Trang chủ</a></li>
            <li><a href="index.php" title="">Dịch vụ</a></li>
            <li><a title="" class="active">Điều trị răng đau</a></li>
        </ul>
    </section>
    <section class="container mb-70s">
        <div class="banner-sevice__details mb-20s">
            <img src="theme/assets/images/img-banner-sevide-1.png">
        </div>
        <ul class="list-sevice__navs">
            <li>
                <a href="dichvuniengrang.php" title="">Niềng răng</a>
            </li>
            <li>
                <a href="dichvunhorangkhon.php" title="">Nhổ răng khôn</a>
            </li>
            <li>
                <a href="dichvuthammirangsu.php" title="">Thẩm mỹ răng sứ</a>
            </li>
            <li>
                <a href="dichvudieutrirangdau.php" title="" class="active">Điều trị răng đau</a>
            </li>
            <li>
                <a href="dichvutrongranggia.php" title="">Trồng răng giả</a>
            </li>
            <li>
                <a href="dichvunhakhoakhac.php" title="">Dịch vụ nha khoa khác</a>
            </li>
        </ul>
    </section>
    <section class="intros-sevice__boxs mb-100s">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="text-about__mains">
                        <h2 class="titles-transform__alls mb-10s color-blues-seconds fs-20s"> dịch vụ </h2>
                        <h3 class="titles-transform__alls titles-bold__alls color-blues-seconds fs-40s mb-30s"> Điều trị răng đau </h3>
                        <div class="text-padding__abouts">
                            <p class="titles-bold__alls color-blues-seconds text-before__mains mb-20s">Đau răng khiến bạn gặp nhiều trở ngại: ăn uống không ngon miệng, khó chịu khi cười nói, phát âm khó khăn, thậm chí khiên cơ thể mệt mỏi</p>
                            <p class="mb-40s">Một trong các nguyên nhân phổ biến gây đau răng là do bệnh nướu, bệnh nha chu. Các vi khuẩn tích tụ trong cao răng hình thành ở đường viền răng và nướu, do đó, cách trị đau răng hiệu quả trong trường hợp này là phối hợp thực hiện cạo vôi răng để loại bỏ hoàn toàn ổ vi khuẩn gây bệnh.</p>
                            <p class="mb-40s">Trường hợp bệnh nha chu tiến triển đã xuất hiện túi nha chu có mủ gây đau nhức răng, bác sĩ cần tiến hành nạo túi nha chu (làm sạch sâu túi nha chu), vệ sinh răng sạch sẽ bằng thiết bị chuyên nghiệp, tạo điều kiện cho nướu liền lại và bao bọc chân răng. Bác sĩ có thể kê thêm thuốc kháng sinh trị viêm cho bệnh nhân. Tuy nhiên có những trường hợp sau khi làm sạch túi nha chu, tình trạng viêm nha chu vẫn không thuyên giảm, phẫu thuật sâu hơn có thể cần can thiệp.</p>
                            <div class="groups-btn__afters">
                                <button data-toggle="modal" data-target="#modal-book__mains" class="btn-blues__second mb-40s"><i class="fa fa-calendar-o" aria-hidden="true"></i> Đặt lịch khám</button>
                                <img src="theme/assets/images/after-btn-alls.png" title="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="slide-service__images ">
                        <div class="sl-service__images swiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-3.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-3.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-3.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-3.png" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                        <div class="group-btns__showss">
                            <div class="showss-button-prev"><img src="theme/assets/images/arow-white-1.png"></div>
                            <div class="showss-button-next"><img src="theme/assets/images/arow-white-1.png"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sevice-catagory__pages mb-50s">
        <div class="container">
            <div class="row gutter-65">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-toothache.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Sâu răng</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-toothache.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Viêm tủy</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-toothache.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Viêm quanh răng
                        </h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-toothache.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Lợi trùm</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-toothache.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Sứt mẻ răng</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container mb-120s">
        <div class="text-introduce__sevices">
            <div class="titles-before__mains mb-50s">
                <h2 class="titles-transform__alls  color-blues-seconds fs-36s"><span class="titles-bold__alls">dịch vụ của</span> chúng tôi</h2>
            </div>
            <div class="intros-text__sevices">
                <p>Bệnh nhân cần được chụp hình để biết được răng khôn mọc đúng hay sai, bác sĩ có thể kê toa thuốc giảm đau và thuốc kháng sinh giảm sưng viêm mô mềm cho bệnh nhân. Trường hợp răng mọc sai cần được nhổ bỏ để tránh ảnh hưởng nghiêm trọng đến các răng khác.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi purus, pretium tristique elit vestibulum. Nunc dictum molestie nibh amet mauris morbi facilisis. Viverra risus eu suscipit pharetra elementum, massa magna nibh lacus. Metus erat quis quisque consectetur eget. Erat fringilla pharetra tristique ut non. Imperdiet velit, vestibulum scelerisque eget posuere magna lectus. Nulla dictum facilisi velit dolor sed purus eget ut neque. Arcu sed sit porttitor faucibus tellus amet amet, cras. Eget orci ut morbi id pellentesque diam arcu eget. A lacus, habitasse tempus justo, consectetur maecenas sit. Vel vitae turpis iaculis ante placerat sit feugiat nisl, facilisi. Metus pretium posuere maecenas facilisis eu ultricies. Eget leo, pharetra est sapien feugiat mus. Urna urna, dui at ridiculus eget tristique cras. A lorem nulla est, ac. Felis, interdum sit accumsan in at sagittis varius. Scelerisque eu convallis eget at rutrum at dui. Sed habitasse tellus pellentesque risus volutpat auctor tincidunt. Elit nam duis sit faucibus egestas posuere erat lacus.</p>
                <br>
                <img src="theme/assets/images/img-bottoms-sevice-texts.png" alt="">
                <br>
                <br>
                <p>Amet, consectetur adipiscing elit. Morbi purus, pretium tristique elit vestibulum. Nunc dictum molestie nibh amet mauris morbi facilisis. Viverra risus eu suscipit pharetra elementum, massa magna nibh lacus. Metus erat quis quisque consectetur eget. Erat fringilla pharetra tristique ut non. Imperdiet velit, vestibulum scelerisque eget posuere magna lectus. Nulla dictum facilisi velit dolor sed purus eget ut neque. Arcu sed sit porttitor faucibus tellus amet amet, cras. Eget orci ut morbi id pellentesque diam arcu eget. A lacus, habitasse tempus justo, consectetur maecenas sit. Vel vitae turpis iaculis ante placerat sit feugiat nisl, facilisi. Metus pretium posuere maecenas facilisis eu ultricies. Eget leo, pharetra est sapien feugiat mus. Urna urna, dui at ridiculus eget tristique cras. A lorem nulla est, ac. Felis, interdum sit accumsan in at sagittis varius. Scelerisque eu convallis eget at rutrum at dui. Sed habitasse tellus pellentesque risus volutpat auctor tincidunt. Elit nam duis sit faucibus egestas posuere erat lacus.</p>
            </div>
            <div class="bottom-news__details">
                <div class="apps-news__details">
                    <h3 class="titles-bold__alls color-blues-seconds fs-16s mb-20s">Chia sẻ</h3>
                    <ul class="app-footers__details">
                        <li>
                            <a href="#" title="">
                                <img src="theme/assets/images/img-app-footer-1.svg" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="#" title="">
                                <img src="theme/assets/images/img-app-footer-2.svg" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="#" title="">
                                <img src="theme/assets/images/img-app-footer-3.svg" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
                <a href="#" class="titles-bold__alls fs-16s names-mains__details" title="">Nha khoa lucci</a>
            </div>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>