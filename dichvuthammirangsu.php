<?php @include('header.php'); ?>
<main>
    <section class="container mb-25s">
        <ul class="breadcrumb">
            <li><a href="index.php" title="">Trang chủ</a></li>
            <li><a href="index.php" title="">Dịch vụ</a></li>
            <li><a title="" class="active">Thẩm mỹ răng sứ</a></li>
        </ul>
    </section>
    <section class="container mb-70s wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="banner-sevice__details mb-20s">
            <img src="theme/assets/images/img-banner-sevide-1.png">
        </div>
        <ul class="list-sevice__navs">
            <li>
                <a href="dichvuniengrang.php" title="">Niềng răng</a>
            </li>
            <li>
                <a href="dichvunhorangkhon.php" title="">Nhổ răng khôn</a>
            </li>
            <li>
                <a href="dichvuthammirangsu.php" title="" class="active">Thẩm mỹ răng sứ</a>
            </li>
            <li>
                <a href="dichvudieutrirangdau.php" title="">Điều trị răng đau</a>
            </li>
            <li>
                <a href="dichvutrongranggia.php" title="">Trồng răng giả</a>
            </li>
            <li>
                <a href="dichvunhakhoakhac.php" title="">Dịch vụ nha khoa khác</a>
            </li>
        </ul>
    </section>
    <section class="intros-sevice__boxs mb-100s">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="text-about__mains">
                        <h2 class="titles-transform__alls mb-10s fs-20s"> dịch vụ </h2>
                        <h3 class="titles-transform__alls titles-bold__alls color-blues-seconds fs-40s mb-30s">Thẩm mỹ răng sứ </h3>
                        <div class="text-padding__abouts">
                            <p class="titles-bold__alls color-blues-seconds text-before__mains mb-20s">Bọc răng sứ thẩm mỹ là phương pháp phục hình bằng vật liệu sứ giúp phục hồi chức năng cải thiện thẩm mỹ, giúp mang lại dáng răng đều, đẹp, màu sắc tự nhiên như răng thật, bảo vệ răng thật trước những loại vi khuẩn gây hại..</p>
                            <p class="mb-40s">Với sự phát triển của công nghệ nha khoa, bọc răng sứ thẩm mỹ không chỉ đem lại một hàm răng trắng sáng tự nhiên. Mà còn giúp bạn khắc phục được các nhược điểm của răng như tình trạng răng hô, móm; phục hồi răng tình trạng răng bị sâu, sứt mẻ, gãy vở; khắc phục các hiện tượng răng mọc không đều, bị xô lệch, răng thưa hở kẽ, răng bị nhiễm màu do kháng sinh hoặc dùng cafe, thuốc lá… giúp mang lại một hàm răng mới đều đẹp như ý muốn của bạn.</p>
                            <p class="mb-40s">Tại nha khoa Lucci, 100% răng sứ cao cấp chính hãng cao cấp nhập khẩu trực tiếp từ Đức đảm bảo kết quả tốt nhất cho khách hàng.
                                Đội ngũ Y bác sĩ chuyên nghiệp tận tâm
                                Trang thiết bị, cơ sở vật chất hiện tại bậc nhất.
                                Chính sách bảo hành và chăm sóc cho khách hàng đến trọn đời.</p>
                            <div class="groups-btn__afters">
                                <button data-toggle="modal" data-target="#modal-book__mains" title="" class="btn-blues__second mb-40s"><i class="fa fa-calendar-o" aria-hidden="true"></i> Đặt lịch khám</button>
                                <img src="theme/assets/images/after-btn-alls.png" title="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="slide-service__images ">
                        <div class="sl-service__images swiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-3.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-3.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-3.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-3.png" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                        <div class="group-btns__showss">
                            <div class="showss-button-prev"><img src="theme/assets/images/arow-white-1.png"></div>
                            <div class="showss-button-next"><img src="theme/assets/images/arow-white-1.png"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sevice-half__bottom sevice-catagory__pages mb-100s wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="container">
            <div class="titles-before__mains mb-50s">
                <h2 class="titles-transform__alls  color-blues-seconds fs-36s"><span class="titles-bold__alls">dịch vụ của</span> chúng tôi</h2>
            </div>
            <div class="row gutter-0">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="img-sevice__half">
                        <img src="theme/assets/images/img-half-sevice-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="items-why__mains">
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s">Bọc răng sứ</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="img-sevice__half">
                        <img src="theme/assets/images/img-half-sevice-2.png" alt="">
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="items-why__mains">
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s">Dán lá sứ</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>