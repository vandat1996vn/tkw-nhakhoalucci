<!DOCTYPE html>
<html itemscope="" itemtype="http://schema.org/WebPage" lang="vi">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="theme/assets/css/animate.css">
    <link rel="stylesheet" href="theme/assets/css/font-awesome.css">
    <link rel="stylesheet" href="theme/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="theme/assets/css/swiper.min.css" />
    <link rel="stylesheet" href="theme/assets/css/select-2.css" />
    <link rel="stylesheet" href="theme/assets/css/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="theme/assets/css/cloudzoom.css">
    <link rel="stylesheet" href="theme/assets/css/main.css">
    <link rel="stylesheet" href="theme/assets/css/abouts.css">
    <link rel="stylesheet" href="theme/assets/css/tintuc.css">
    <link rel="stylesheet" href="theme/assets/css/dichvu.css">
    <link rel="stylesheet" href="theme/assets/css/bang-gia.css">
    <link rel="stylesheet" href="theme/assets/css/lienhe.css">
    <link rel="stylesheet" href="theme/assets/css/reset.css">
    <link rel="stylesheet" href="theme/assets/css/responsive.css">
</head>

<body>
    <header>
        <section class="header">
            <div class="container">
                <div class="logo-mains">
                    <a href="index.php"><img alt="" src="theme/assets/images/logo-mains.png"></a>
                </div>
                <div class="menu">
                    <div class="content-menus menu-desktop">
                        <div class="header-menu-mobile">
                            <a href="index.php" title="Trang chủ" class="logo-menu__mobile mb-10s"> <img alt="" alt="" src="theme/assets/images/logo-mains.png" class="img-fluid"></a>
                        </div>
                        <ul>
                            <li>
                                <a href="index.php" title="">Trang chủ</a>
                            </li>
                            <li>
                                <a href="abouts.php" title="">Giới thiệu</a>
                            </li>
                            <li>
                                <a href="dichvunhakhoakhac.php" title="">Dịch vụ</a>
                                <ul>
                                    <li>
                                        <a href="dichvuniengrang.php" title="" class="">Niềng răng</a>
                                    </li>
                                    <li>
                                        <a href="dichvunhorangkhon.php" title="">Nhổ răng khôn</a>
                                    </li>
                                    <li>
                                        <a href="dichvuthammirangsu.php" title="">Thẩm mỹ răng sứ</a>
                                    </li>
                                    <li>
                                        <a href="dichvudieutrirangdau.php" title="">Điều trị răng đau</a>
                                    </li>
                                    <li>
                                        <a href="dichvutrongranggia.php" title="">Trồng răng giả</a>
                                    </li>
                                    <li>
                                        <a href="dichvunhakhoakhac.php" title="">Dịch vụ nha khoa khác</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="banggia.php" title="">Bảng giá</a>
                            </li>
                            <li>
                                <a href="tintuc-cabenhnoibat.php" title="">Tin tức</a>
                                <ul>
                                    <li>
                                        <a href="tintuc-kienthucnhakhoa.php" title="">Kiến thức nha khoa</a>
                                    </li>
                                    <li>
                                        <a href="tintuc-cabenhnoibat.php" title="">Ca bệnh nổi bật</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="contacts.php" title="">Liên hệ</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="right-headers">
                    <a href="#" title="" class="phones-rights__headers titles-bold__alls color-blues"><img src="theme/assets/images/phone-headers.svg"> 1-800 123-456</a>
                    <button href="" data-toggle="modal" data-target="#modal-book__mains" class="btn-blues__alls"><i class="fa fa-calendar-o" aria-hidden="true"></i> đặt lịch khám</button>
                </div>
                <div class="bg-over-menu"></div>
                <span class="button-phone btn_sp_menu"><img src="theme/assets/images/nav-btn-menu.png" alt=""></span>
            </div>
        </section>
    </header>