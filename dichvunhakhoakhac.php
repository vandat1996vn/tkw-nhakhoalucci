<?php @include('header.php'); ?>
<main>
    <section class="container mb-25s">
        <ul class="breadcrumb">
            <li><a href="index.php" title="">Trang chủ</a></li>
            <li><a href="index.php" title="">Dịch vụ</a></li>
            <li><a href="index.php" title="">Niềng răng</a></li>
            <li><a title="" class="active">Niềng răng bằng mắc cài kim loại</a></li>
        </ul>
    </section>
    <section class="container mb-70s wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="banner-sevice__details mb-20s">
            <img src="theme/assets/images/img-banner-sevide-1.png">
        </div>
        <ul class="list-sevice__navs">
            <li>
                <a href="dichvuniengrang.php" title="">Niềng răng</a>
            </li>
            <li>
                <a href="dichvunhorangkhon.php" title="">Nhổ răng khôn</a>
            </li>
            <li>
                <a href="dichvuthammirangsu.php" title="">Thẩm mỹ răng sứ</a>
            </li>
            <li>
                <a href="dichvudieutrirangdau.php" title="">Điều trị răng đau</a>
            </li>
            <li>
                <a href="dichvutrongranggia.php" title="">Trồng răng giả</a>
            </li>
            <li>
                <a href="dichvunhakhoakhac.php" title="" class="active">Dịch vụ nha khoa khác</a>
            </li>
        </ul>
    </section>
    <section class="content-sevice__news mb-100s wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-4">
                    <div class="left-post__sevices">
                        <ul>
                            <li>
                                <a href="#" title="" class="active">Nhổ răng bệnh lý</a>
                            </li>
                            <li>
                                <a href="#" title="">Hàn răng</a>
                            </li>
                            <li>
                                <a href="#" title="">Vệ sinh răng (lấy cao răng) </a>
                            </li>
                            <li>
                                <a href="#" title="">Bọc, chụp răng sứ </a>
                            </li>
                            <li>
                                <a href="#" title="">Trồng răng bằng cầu răng sứ cố định </a>
                            </li>
                            <li>
                                <a href="#" title="">Trồng răng giả tháo lắp </a>
                            </li>
                            <li>
                                <a href="#" title="">Implant</a>
                            </li>
                            <li>
                                <a href="#" title="">Điều trị nha chu</a>
                            </li>
                            <li>
                                <a href="#" title="">Điều trị viêm lợi bằng máy Laser </a>
                            </li>
                            <li>
                                <a href="#" title="">Cắt thắng môi, cắt phanh lưỡi</a>
                            </li>
                            <li>
                                <a href="#" title="">Tẩy trắng răng </a>
                            </li>
                            <li>
                                <a href="#" title="">Cắt nang chân răng </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="text-post__sevices">
                        <div class="titles-before__mains mb-50s">
                            <h2 class="titles-transform__alls  color-blues-seconds fs-36s"><span class="titles-bold__alls">dịch vụ của</span> chúng tôi</h2>
                        </div>
                        <div class="intros-text__sevices">
                            <p>Bệnh nhân cần được chụp hình để biết được răng khôn mọc đúng hay sai, bác sĩ có thể kê toa thuốc giảm đau và thuốc kháng sinh giảm sưng viêm mô mềm cho bệnh nhân. Trường hợp răng mọc sai cần được nhổ bỏ để tránh ảnh hưởng nghiêm trọng đến các răng khác.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi purus, pretium tristique elit vestibulum. Nunc dictum molestie nibh amet mauris morbi facilisis. Viverra risus eu suscipit pharetra elementum, massa magna nibh lacus. Metus erat quis quisque consectetur eget. Erat fringilla pharetra tristique ut non. Imperdiet velit, vestibulum scelerisque eget posuere magna lectus. Nulla dictum facilisi velit dolor sed purus eget ut neque. Arcu sed sit porttitor faucibus tellus amet amet, cras. Eget orci ut morbi id pellentesque diam arcu eget. A lacus, habitasse tempus justo, consectetur maecenas sit. Vel vitae turpis iaculis ante placerat sit feugiat nisl, facilisi. Metus pretium posuere maecenas facilisis eu ultricies. Eget leo, pharetra est sapien feugiat mus. Urna urna, dui at ridiculus eget tristique cras. A lorem nulla est, ac. Felis, interdum sit accumsan in at sagittis varius. Scelerisque eu convallis eget at rutrum at dui. Sed habitasse tellus pellentesque risus volutpat auctor tincidunt. Elit nam duis sit faucibus egestas posuere erat lacus.</p>
                            <br>
                            <img src="theme/assets/images/img-bottoms-sevice-texts.png" alt="">
                            <br>
                            <br>
                            <p>Amet, consectetur adipiscing elit. Morbi purus, pretium tristique elit vestibulum. Nunc dictum molestie nibh amet mauris morbi facilisis. Viverra risus eu suscipit pharetra elementum, massa magna nibh lacus. Metus erat quis quisque consectetur eget. Erat fringilla pharetra tristique ut non. Imperdiet velit, vestibulum scelerisque eget posuere magna lectus. Nulla dictum facilisi velit dolor sed purus eget ut neque. Arcu sed sit porttitor faucibus tellus amet amet, cras. Eget orci ut morbi id pellentesque diam arcu eget. A lacus, habitasse tempus justo, consectetur maecenas sit. Vel vitae turpis iaculis ante placerat sit feugiat nisl, facilisi. Metus pretium posuere maecenas facilisis eu ultricies. Eget leo, pharetra est sapien feugiat mus. Urna urna, dui at ridiculus eget tristique cras. A lorem nulla est, ac. Felis, interdum sit accumsan in at sagittis varius. Scelerisque eu convallis eget at rutrum at dui. Sed habitasse tellus pellentesque risus volutpat auctor tincidunt. Elit nam duis sit faucibus egestas posuere erat lacus.</p>
                        </div>
                        <div class="bottom-news__details">
                            <div class="apps-news__details">
                                <h3 class="titles-bold__alls color-blues-seconds fs-16s mb-20s">Chia sẻ</h3>
                                <ul class="app-footers__details">
                                    <li>
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-app-footer-1.svg" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-app-footer-2.svg" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-app-footer-3.svg" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <a href="#" class="titles-bold__alls fs-16s names-mains__details" title="">Nha khoa lucci</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>