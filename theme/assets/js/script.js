var GUI = (function() {
    var win = $(window);
    var html = $('html,body');

    var menuMobile = function() {

        $(document).ready(function() {
            $(window).resize(function() {
                if (win.width() < 992) {
                    $('.content-menus').removeClass("menu-desktop");
                    $('.content-menus').addClass("main-menu-mobile");
                } else {
                    $('.content-menus').addClass("menu-desktop");
                    $('.content-menus').removeClass("main-menu-mobile");
                }
            });
        });

        if (win.width() < 992) {
            $('.content-menus').removeClass("menu-desktop");
            $('.content-menus').addClass("main-menu-mobile");
        } else {
            $('.content-menus').addClass("menu-desktop");
            $('.content-menus').removeClass("main-menu-mobile");
        }



        $(document).ready(function($) {
            $('.button-phone').on('click', function(event) {
                $('.animated-icon1').toggleClass('open');
                $('.bg-over-menu').toggleClass('show-over');
                $('.main-menu-mobile').toggleClass('active-menu-mobile');
                $('body').toggleClass('overflow-hidden');
                $('.close-menu-btn').addClass("active-close__menussss");
                $('.content-header').toggleClass("active-content__header");
            });
            $('.btn-menu__mopbiles').on('click', function(event) {
                $('.animated-icon1').toggleClass('open');
                $('.bg-over-menu').toggleClass('show-over');
                $('.main-menu-mobile').toggleClass('active-menu-mobile');
                $('body').toggleClass('overflow-hidden');
                $('.close-menu-btn').addClass("active-close__menussss");
                $('.content-header').toggleClass("active-content__header");
            });
            $('.bg-over-menu').on('click', function(event) {
                $('.animated-icon1').toggleClass('open');
                $('.bg-over-menu').toggleClass('show-over');
                $('.main-menu-mobile').removeClass('active-menu-mobile');
                $('body').toggleClass('overflow-hidden');
                $('.close-menu-btn').removeClass("active-close__menussss");
                $('.content-header').removeClass("active-content__header");
                $('.main-menu-mobile ul').find('li i').removeClass("active");
                $('.main-menu-mobile ul').find('li').removeClass("actives-li__mobiles");
                $('.main-menu-mobile ul').find('ul').slideUp("fast");
            });
            $('.close-menu-btn').on('click', function(event) {
                $('.animated-icon1').toggleClass('open');
                $('.bg-over-menu').toggleClass('show-over');
                $('.main-menu-mobile').removeClass('active-menu-mobile');
                $('body').toggleClass('overflow-hidden');
                $(this).removeClass("active-close__menussss");
                $('.content-header').removeClass("active-content__header");
                $('.main-menu-mobile ul').find('li i').removeClass("active");
                $('.main-menu-mobile ul').find('li').removeClass("actives-li__mobiles");
                $('.main-menu-mobile ul').find('ul').slideUp("fast");
            });
        });

        $('.main-menu-mobile').find("ul li").each(function() {
            if ($(this).find("ul>li").length > 0) {
                $(this).prepend('<i></i>');
                $(this).append('<span></span');
            }
        });

        $('.menu-desktop').find("ul li").each(function() {
            if ($(this).find("ul>li").length > 0) {
                $(this).append('<i></i>');
            }
        });

        $('.main-menu-mobile ul').find('li i').click(function(event) {
            var ul = $(this).nextAll("ul");
            if (ul.is(":hidden")) {
                $(this).addClass('active');
                ul.slideDown(200);
            } else {
                $(this).removeClass('active');
                ul.slideUp();
            }
        });
    };

    var _scroll_menus = function() {
        var win = $(window);
        var heighttops = $('.header').outerHeight();
        var prevScrollpos = window.pageYOffset;

        $('body').css('padding-top', heighttops);

        if ($("body").find(".content-prds__details").length > 0) {
            $("header").addClass("header-prds__details");
        }

        window.onscroll = function() {

            var currentScrollPos = window.pageYOffset;

            if (win.scrollTop() >= heighttops) {

                $('.header').addClass('scroll-fixed__headers');

                $('.btn-fix__rights').addClass('active-fix__rights');



                if (win.width() <= 575) {


                    if (prevScrollpos > currentScrollPos) {

                        $('.header').css('top', 0);
                    } else {

                        $('.header').css('top', (0 - heighttops));
                    }
                    prevScrollpos = currentScrollPos;
                }

            } else {
                $('.header').css('top', 0);
                $('.header').removeClass('scroll-fixed__headers');
                $('.btn-fix__rights').removeClass('active-fix__rights');
            }
        }
    };

    var selectAll = function() {
        $(document).ready(function() {
            if ($('.select-alls').length > 0) {
                $('.select-alls').select2();
            }
        });

        // $('#modal-guides__accounts').modal('show');

    };

    var controlDate = function() {
        $(".control-date input").click(function() {
            $(this).parent(".control-date").addClass("active-input__texts");
            $(this).val(new Date().toJSON().slice(0, 10));

        });

        $('.control-date input').blur(function() {
            tmpval = $(this).val();
            if (tmpval == '') {
                $(this).parent(".control-date").removeClass("active-input__texts");
            } else {
                $(this).parent(".control-date").addClass("active-input__texts");
            }
        });
    };

    var btnPriceSelects = function() {
        $(".btn-classify__prices").click(function() {
            $(this).find(".list-classify__pricess").fadeToggle();

        });
    };

    var scrollTopss = function() {
        $(".btn-up__tops").click(function() {
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
    };

    var slideCustomerMains = function() {
        if ($('.sl-customer__mains').length === 0) return;
        var swiper2 = new Swiper('.sl-customer__mains', {
            slidesPerView: 1,
            spaceBetween: 0,
            loop: true,
            draggable: true,
            autoplay: {
                delay: 5000,
            },
            pagination: {
                el: ".swiper-pagination",
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        });


        $(".showss-button-prev").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-prev").trigger("click");
        });

        $(".showss-button-next").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-next").trigger("click");
        });
    };

    var slideSeviceImages = function() {
        if ($('.sl-service__images').length === 0) return;
        var swiper2 = new Swiper('.sl-service__images', {
            slidesPerView: 1,
            spaceBetween: 0,
            loop: true,
            draggable: true,
            autoplay: {
                delay: 5000,
            },
            pagination: {
                el: ".swiper-pagination",
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        });


        $(".showss-button-prev").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-prev").trigger("click");
        });

        $(".showss-button-next").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-next").trigger("click");
        });
    };


    var slideNewsMains = function() {
        if ($('.sl-news__mains').length === 0) return;
        var swiper2 = new Swiper('.sl-news__mains', {
            slidesPerView: 3,
            spaceBetween: 50,
            loop: true,
            draggable: true,
            autoplay: {
                delay: 5000,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 5
                },
                576: {
                    slidesPerView: 2,
                    spaceBetween: 5
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                992: {
                    slidesPerView: 3,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: 3,
                    spaceBetween: 25
                },
                1440: {
                    slidesPerView: 3,
                    spaceBetween: 25
                }
            }

        });


        $(".showss-button-prev").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-prev").trigger("click");
        });

        $(".showss-button-next").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-next").trigger("click");
        });
    };

    var slideAboutsVisions = function() {
        if ($('.sl-abouts__visions').length === 0) return;
        var swiper2 = new Swiper('.sl-abouts__visions', {
            slidesPerView: 1,
            spaceBetween: 0,
            loop: true,
            draggable: true,
            autoplay: {
                delay: 5000,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: ".swiper-pagination",
            },
        });


        $(".showss-button-prev").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-prev").trigger("click");
        });

        $(".showss-button-next").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-next").trigger("click");
        });
    };

    var slideGuiedModals = function() {
        var swiper = new Swiper(".sl-thums__guides", {
            spaceBetween: 3,
            slidesPerView: 15,
            freeMode: true,
            watchSlidesProgress: true,
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 4,
                    spaceBetween: 10,
                },
                576: {
                    slidesPerView: 4,
                    spaceBetween: 10,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 10
                },
                992: {
                    slidesPerView: 4,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: 5,
                    spaceBetween: 15
                },
                1440: {
                    slidesPerView: 5,
                    spaceBetween: 15
                }
            }
        });

        var swiper2 = new Swiper('.sl-guides__accounts', {
            pagination: {
                el: '.swiper-pagination',
                type: 'progressbar',
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            thumbs: {
                swiper: swiper,
            },
        });


        var counter = $('.fraction');
        var currentCount = $('<span class="count">1<span/>');
        counter.append(currentCount);

        swiper2.on('transitionStart', function() {
            var index = this.activeIndex + 1,
                $current = $(".photo-slide").eq(index),
                $c_cur = $("#count_cur"),
                $c_next = $("#count_next"),
                dur = 0.8;

            var prevCount = $('.count');
            currentCount = $('<span class="count next">' + index + '<span/>');
            counter.html(currentCount);
        });

        var numberSwiper = $(".sl-guides__accounts").find(".swiper-slide").length;
        $('.all-items__swipers').text(numberSwiper);

        $(".slide-guides__accounts .showss-button-prev").click(function() {
            $(this).parent().parent().find(".swiper .swiper-button-prev").trigger("click");
        });

        $(".slide-guides__accounts .showss-button-next").click(function() {
            $(this).parent().parent().find(".swiper .swiper-button-next").trigger("click");
        });
    };

    var slideImagesVisions = function() {
        if ($('.sl-images__visions').length === 0) return;
        var swiper2 = new Swiper('.sl-images__visions', {
            slidesPerView: "auto",
            spaceBetween: 10,
            loop: true,
            centeredSlides: true,
            draggable: true,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
        });
    };


    var allHeightssssss = async function() {
        var win = await $(window);

        $('.sl-news__mains .swiper-wrapper').each(function() {
            var highestBoxx = 0;
            $(this).find('.swiper-slide .items-news__mains').each(function() {
                if ($(this).height() > highestBoxx) {
                    highestBoxx = $(this).height();
                }
            })
            $(this).find('.swiper-slide .items-news__mains').height(highestBoxx);
        });

        // $('#modal-book__mains').modal('show');
    };

    var scrollTopss = function() {
        $(".up-to__tops").click(function() {
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
    };

    var initWowJs = function() {
        new WOW().init();
    };

    return {
        _: function() {
            menuMobile();
            _scroll_menus();
            allHeightssssss();
            selectAll();
            controlDate();
            btnPriceSelects();
            slideGuiedModals();
            slideCustomerMains();
            slideNewsMains();
            slideAboutsVisions();
            slideImagesVisions();
            slideSeviceImages();
            initWowJs();
            scrollTopss();
        }
    };


})();
$(document).ready(function() {
    // if (/Lighthouse/.test(navigator.userAgent)) {
    //     return;
    // }
    GUI._();
});