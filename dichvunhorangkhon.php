<?php @include('header.php'); ?>
<main>
    <section class="container mb-25s">
        <ul class="breadcrumb">
            <li><a href="index.php" title="">Trang chủ</a></li>
            <li><a href="index.php" title="">Dịch vụ</a></li>
            <li><a title="" class="active">Nhổ răng khôn</a></li>
        </ul>
    </section>
    <section class="container mb-70s wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="banner-sevice__details mb-20s">
            <img src="theme/assets/images/img-banner-sevide-1.png">
        </div>
        <ul class="list-sevice__navs">
            <li>
                <a href="dichvuniengrang.php" title="">Niềng răng</a>
            </li>
            <li>
                <a href="dichvunhorangkhon.php" class="active" title="">Nhổ răng khôn</a>
            </li>
            <li>
                <a href="dichvuthammirangsu.php" title="">Thẩm mỹ răng sứ</a>
            </li>
            <li>
                <a href="dichvudieutrirangdau.php" title="">Điều trị răng đau</a>
            </li>
            <li>
                <a href="dichvutrongranggia.php" title="">Trồng răng giả</a>
            </li>
            <li>
                <a href="dichvunhakhoakhac.php" title="" class="active">Dịch vụ nha khoa khác</a>
            </li>
        </ul>
    </section>
    <section class="intros-sevice__boxs mb-100s">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="text-about__mains">
                        <h2 class="titles-transform__alls mb-10s fs-20s"> dịch vụ </h2>
                        <h3 class="titles-transform__alls titles-bold__alls color-blues-seconds fs-40s mb-30s"> nhổ răng khôn </h3>
                        <div class="text-padding__abouts">
                            <p class="titles-bold__alls color-blues-seconds text-before__mains mb-20s">Nisl vitae quisque sapien id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper arcu quis dui nunc at blandit tincidunt in viverra.</p>
                            <p class="mb-40s">Tuy nhiên, đây lại là một trong những dịch vụ mang đến nỗi ám ảnh cho nhiều người bởi ai cũng sợ đau và các kỹ thuật trước kia thực sự chưa chuyên nghiệp.
                                Nhổ răng là chỉ định bắt buộc khi chiếc răng hỏng ảnh hưởng không tốt đến sức khỏe và các răng khác. Đây là thủ thuật cơ bản nhưng đòi hỏi chuyên môn được đào tạo đúng chuẩn của bác sĩ và các thiết bị tiên tiến mới đem lại sự an toàn tuyệt đối và hiệu quả cho sức khỏe bệnh nhân.</p>
                            <p class="mb-40s">Nếu quý khách đang muốn tìm kiếm cho mình một dịch vụ nhổ răng không đau, được chăm sóc bởi các bác sĩ có tay nghề cao. Vậy Nha Khoa Quốc Tế Phú Hòa có thể sẽ là lựa chọn của bạn.</p>
                            <div class="groups-btn__afters">
                                <button class="btn-blues__second mb-40s" data-toggle="modal" data-target="#modal-book__mains"><i class="fa fa-calendar-o" aria-hidden="true"></i> Đặt lịch khám</button>
                                <img src="theme/assets/images/after-btn-alls.png" title="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="slide-service__images ">
                        <div class="sl-service__images swiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-3.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-3.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-3.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-3.png" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                        <div class="group-btns__showss">
                            <div class="showss-button-prev"><img src="theme/assets/images/arow-white-1.png"></div>
                            <div class="showss-button-next"><img src="theme/assets/images/arow-white-1.png"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sevice-catagory__pages mb-70s wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="container">
            <div class="row gutter-65">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-sevice-abouts-4.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Gây tê không đau, máy kỹ thuật số</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-sevice-abouts-4.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Máy nhổ răng piezotome</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-sevice-abouts-4.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Nhổ hàng ngăn răng
                        </h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sevice-hot__mains mb-100s wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="list-sevice__hots">
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-1.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-2.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-3.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-4.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-5.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-6.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-7.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-8.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-9.png">
            </a>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>