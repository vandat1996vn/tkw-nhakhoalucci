<?php @include('header.php'); ?>
<main>
    <section class="container mb-25s">
        <ul class="breadcrumb">
            <li><a href="index.php" title="">Trang chủ</a></li>
            <li><a title="" class="active">Liên hệ</a></li>
        </ul>
    </section>
    <section class="content-contacts__pages mb-150s">
        <div class="container">
            <h2 class="titles-bold__alls titles-transform__alls color-blues-seconds fs-48s mb-50s">Liên hệ</h2>
            <div class="row">
                <div class="col-lg-5 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="box-left__contacts">
                        <div class="tops-contacts__lefts">
                            <div class="intros-tops__contacts">
                                <h3 class="titles-bold__alls fs-24s">Nha khoa Lucci</h3>
                                <ul class="mb-15s">
                                    <li>
                                        <h3 class="titles-bold__alls fs-18s mb-15s">Trụ sở:</h3>
                                        <p>50 ngõ 14 Vũ Hữu, Quận Thanh Xuân, Hà Nội </p>
                                    </li>
                                    <li>
                                        <h3 class="titles-bold__alls fs-18s mb-15s">Email:</h3>
                                        <p><a href="#" title="">vanhien.nhakhoa@gmail.com</a> </p>
                                    </li>
                                </ul>
                                <a href="#" title="" class="phone-contacts__pages"><img src="theme/assets/images/phone-headers.svg"> 0918.382.228</a>
                            </div>
                            <img src="theme/assets/images/logo-mains-contacts.png" alt="">
                        </div>
                        <ul class="apps-contacts__pages">
                            <li>
                                <a href="#" title=""> <img src="theme/assets/images/img-app-contacts-1.png" alt=""> </a>
                            </li>
                            <li>
                                <a href="#" title=""> <img src="theme/assets/images/img-app-contacts-2.png" alt=""> </a>
                            </li>
                            <li>
                                <a href="#" title=""> <img src="theme/assets/images/img-app-contacts-3.png" alt=""> </a>
                            </li>
                            <li>
                                <a href="#" title=""> <img src="theme/assets/images/img-app-contacts-4.png" alt=""> </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-7 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="box-right__contacts">
                        <div class="row gutter-0">
                            <div class="col-lg-6">
                                <div class="infos-rights__contacts">
                                    <h3 class="titles-bold__alls color-blues-seconds fs-24s">Giờ làm việc</h3>
                                    <ul>
                                        <li>
                                            <h3 class="titles-bold__alls fs-18s mb-15s">Thứ 2 - Thứ 6</h3>
                                            <p>Từ 8h00 - 20h00</p>
                                        </li>
                                        <li>
                                            <h3 class="titles-bold__alls fs-18s mb-15s">Thứ 7</h3>
                                            <p>Từ 8h00 - 17h00</p>
                                        </li>
                                        <li>
                                            <h3 class="titles-bold__alls fs-18s">Chủ Nhật - Nghỉ</h3>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="maps-contacts___details">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d930.8874854112767!2d105.8047464!3d21.0506866!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1663121321270!5m2!1svi!2s" width="" height="" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="book-calendar__contacts wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="container">
            <form>
                <h2 class="titles-bold__alls titles-transform__alls titles-center__alls color-blues-seconds fs-48s mb-60s">Đặt lịch khám</h2>
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <div class="form-groups__book">
                            <input type="text" name="" placeholder="Tên của bạn*" class="control-alls input-alls">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-groups__book">
                            <input type="text" name="" placeholder="Số điện thoại*" class="control-alls input-alls">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-groups__book">
                            <input type="text" name="" placeholder="Độ tuổi" class="control-alls input-alls">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="control-select__alls">
                            <select class="select-alls">
                                <option value="">Dịch vụ*</option>
                                <option value="1">Niềng răng</option>
                                <option value="2">Nhổ răng khôn</option>
                                <option value="3">Thẩm mỹ răng sứ</option>
                                <option value="3">Điều trị răng đau</option>
                                <option value="3">Trồng răng giả</option>
                                <option value="3">Dịch vụ nha khoa khác</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="control-groups__accounts mb-10s control-date">
                            <input placeholder="Ngày" class="textbox-n" type="date" />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="control-select__alls">
                            <select class="select-alls">
                                <option value="">Giờ</option>
                                <option value="1">19 giờ</option>
                                <option value="2">20 giờ</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="5" class="control-alls control-texts" placeholder="Tình trạng"></textarea>
                    </div>
                </div>
                <button class="btn-oranges__alls"><i class="fa fa-calendar-o" aria-hidden="true"></i> Đặt lịch</button>
                <div class="bg-contacts__pages">
                    <img src="theme/assets/images/bg-contacts-pages-form.png" alt="">
                </div>
            </form>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>