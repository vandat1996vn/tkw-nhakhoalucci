<?php @include('header.php'); ?>
<main>
    <section class="container mb-25s">
        <ul class="breadcrumb">
            <li><a href="index.php" title="">Trang chủ</a></li>
            <li><a href="index.php" title="">Dịch vụ</a></li>
            <li><a title="" class="active">Niềng răng</a></li>
        </ul>
    </section>
    <section class="container mb-70s wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="banner-sevice__details mb-20s">
            <img src="theme/assets/images/img-banner-sevide-1.png">
        </div>
        <ul class="list-sevice__navs">
            <li>
                <a href="dichvuniengrang.php" title="" class="active">Niềng răng</a>
            </li>
            <li>
                <a href="dichvunhorangkhon.php" title="">Nhổ răng khôn</a>
            </li>
            <li>
                <a href="dichvuthammirangsu.php" title="">Thẩm mỹ răng sứ</a>
            </li>
            <li>
                <a href="dichvudieutrirangdau.php" title="">Điều trị răng đau</a>
            </li>
            <li>
                <a href="dichvutrongranggia.php" title="">Trồng răng giả</a>
            </li>
            <li>
                <a href="dichvunhakhoakhac" title="">Dịch vụ nha khoa khác</a>
            </li>
        </ul>
    </section>
    <section class="intros-sevice__boxs mb-100s">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="text-about__mains">
                        <h2 class="titles-transform__alls mb-10s fs-20s"> dịch vụ </h2>
                        <h3 class="titles-transform__alls titles-bold__alls color-blues-seconds fs-40s mb-30s"> Niềng răng mắc cài </h3>
                        <div class="text-padding__abouts">
                            <p class="titles-bold__alls color-blues-seconds text-before__mains mb-20s">Nisl vitae quisque sapien id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper arcu quis dui nunc at blandit tincidunt in viverra.</p>
                            <p class="mb-40s">Hiện này, tồn tại rất nhiều phương pháp niềng răng khác nhau. Nếu dịch vụ niềng răng trong suốt Invisalign nổi lên với tính thẩm mỹ cao thì dịch vụ niềng răng mắc cài vẫn được phần đông người dân sử dụng bởi tính đơn giản, truyền thống và tiết kiệm chi phí. Nếu bạn đang phân vẫn chưa biết lựa chọn phương pháp niềng răng nào cho riêng mình, hãy đến với Nha Khoa Lucci để chúng tôi có thể trực tiếp tư vấn cho bạn.</p>
                            <div class="groups-btn__afters">
                                <button href="#" title="" class="btn-blues__second mb-40s" data-toggle="modal" data-target="#modal-book__mains"><i class="fa fa-calendar-o" aria-hidden="true"></i> Đặt lịch khám</button>
                                <img src="theme/assets/images/after-btn-alls.png" title="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="slide-service__images ">
                        <div class="sl-service__images swiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-1.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-1.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-1.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-1.png" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                        <div class="group-btns__showss">
                            <div class="showss-button-prev"><img src="theme/assets/images/arow-white-1.png"></div>
                            <div class="showss-button-next"><img src="theme/assets/images/arow-white-1.png"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sevice-catagory__pages mb-70s wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="container">
            <div class="row gutter-65">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-why-mains-1.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Mắc cài kim loại</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-why-mains-1.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Mắc cài kim loại thông minh</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-why-mains-1.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Mắc cài trong suốt ( pha lê, sứ)</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="intros-sevice__boxs mb-100s">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="text-about__mains">
                        <h2 class="titles-transform__alls mb-10s fs-20s"> dịch vụ </h2>
                        <h3 class="titles-transform__alls titles-bold__alls color-blues-seconds fs-40s mb-30s"> Niềng răng máng </h3>
                        <div class="text-padding__abouts">
                            <p class="titles-bold__alls color-blues-seconds text-before__mains mb-20s">Nisl vitae quisque sapien id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper arcu quis dui nunc at blandit tincidunt in viverra.</p>
                            <p class="mb-40s">Niềng răng invisalign là phương pháp niềng răng hiện đại, sử dụng mắc cài có thể tháo lắp linh hoạt. Nguyên lý hoạt động không dựa trên mắc cài. Sử dụng chủ yếu là các khay niềng trong suốt, lực trên khay tác động lên răng. Sau đó, sẽ làm răng di chuyển về đúng vị trí của nó.
                                Theo các chuyên gia nha khoa y tế, invisalign là phương pháp chỉnh răng vượt bậc trong tất cả các phương pháp hiện nay..</p>
                            <div class="groups-btn__afters">
                                <button data-toggle="modal" data-target="#modal-book__mains" class="btn-blues__second mb-40s"><i class="fa fa-calendar-o" aria-hidden="true"></i> Đặt lịch khám</button>
                                <img src="theme/assets/images/after-btn-alls.png" title="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.1s">
                    <div class="slide-service__images ">
                        <div class="sl-service__images swiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-2.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-2.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-2.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="items-images__sevices">
                                        <a href="#" title="">
                                            <img src="theme/assets/images/img-sevice-text-pages-2.png" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                        <div class="group-btns__showss">
                            <div class="showss-button-prev"><img src="theme/assets/images/arow-white-1.png"></div>
                            <div class="showss-button-next"><img src="theme/assets/images/arow-white-1.png"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sevice-catagory__pages mb-70s wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="container">
            <div class="row gutter-65">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-why-mains-1.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Máng Leetray
                            thương hiệu Việt</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-why-mains-1.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Máng Reveal
                            thương hiệu Mỹ</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="items-why__mains">
                        <div class="img-why__mains mb-25s">
                            <img src="theme/assets/images/img-why-mains-1.png" alt="">
                        </div>
                        <h3 class="titles-bold__alls color-blues-seconds fs-20s mb-20s">Máng Invisalign
                            thương hiệu Mỹ</h3>
                        <div class="text-why__mains">
                            <p>Với phương pháp mắc cài tự buộc, dây cao su trước đây sẽ được thay thế bằng các nắp trượt thông minh để giữ dây cung cố định trong mắc cài. Từ đó lực ma sát với răng cũng được giảm tối đa, thun kim loại cũng ít bị biến dạng hơn... </p>
                        </div>
                        <a href="dichvunhakhoabaiviet.php" class="btn-blues__alls">Xem chi tiết</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sevice-hot__mains mb-100s wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
        <div class="list-sevice__hots">
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-1.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-2.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-3.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-4.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-5.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-6.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-7.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-8.png">
            </a>
            <a data-toggle="modal" data-target="#modal-guides__accounts" title="" class="items-sevices__hots">
                <div class="intros-sevice__hots">
                    <h3 class="titles-bold__alls fs-16s titles-transform__alls">Khách hàng làm răng</h3>
                </div>
                <img src="theme/assets/images/img-hot-sevice-9.png">
            </a>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>